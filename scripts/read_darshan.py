﻿import sys
import commands
import os
import os.path
import pprint
import json
import argparse

# script que realiza um parsed do arquivo parseado do Darshan 2.0
# recebe com parametro o arquivo a ser parseado
# retorna as informacoes de tempo de IO de cada fase, tempo entre as fases
# as fases pode ser forma coletiva ou nao 



# funcao que le  dados do cabecalho do aquivo
# já parseado do Darshan, recebe  como parametro o arquivo
# e retorna um dicionario com os dados
def read_darshan_header(arq):
	linha = arq.readline()
	while not (("jobid" in linha)):
		linha = arq.readline()
	jobid = linha.split(':')[1]
	while not (("start_time_asci:" in linha)):
		linha = arq.readline()
	start_time_asci = linha.split(':')[1]
	while not (("end_time_asci:" in linha)):
		linha = arq.readline()
	end_time_asci = linha.split(':')[1]
	while not (("nprocs:" in linha)):
		linha = arq.readline()	
	nprocs = linha.split(':')[1]
	while not (("run time:" in linha)):
		linha = arq.readline()
	runtime = linha.split(':')[1]
	dic_header = {}
	dic_header['jobid'] = jobid
	dic_header['start_time_asci'] = start_time_asci
	dic_header['end_time_asci'] = end_time_asci
	dic_header['nprocs'] = nprocs
	dic_header['run_time'] = runtime
	return dic_header

# funcao que le os valores dos counters do arquivo parseado do darshan
# recebe por parametro o aquivo e uma lista com os counters
# retorna um dicionario com todos os valores, organizados da seguinte forma:
#
#filename
# 	|_rank
#		|_operation
#			|_start
#			|_end
#			|_duration
def read_darshan_file(arq,counters):
	dic_contents ={}
	arq.seek(0, 0)
	linha = arq.readline()
	while not (("#<rank>" in linha)):
		linha = arq.readline()	
	while (linha):
		linha = arq.readline()
		if len(linha) == 0:
			continue
		rank, file, counter, value, lixo, lixo, fs = linha.split()
		if counter in counters:
			if "READ" in counter:
			 	operation = "read"
			elif "WRITE" in counter:
			  	operation = "write"
			if "START" in counter:
				if not file in dic_contents:
					dic_contents[file] = {}
				if not rank in dic_contents[file]:
					dic_contents[file][rank] = {}
				if not operation in dic_contents[file][rank]:
					dic_contents[file][rank][operation] = {}

## conferir rse nao esta sobrecremvento

				try:
					dic_contents[file][rank][operation]['start'] = value
				except Exception:
					print "PANIC! Error Reading Data From 'dic_contents'"		
			elif "END" in counter:
				dic_contents[file][rank][operation]["end"] = value
				dic_contents[file][rank][operation]["duration"] = float(value) - float(dic_contents[file][rank][operation]["start"])
				dic_contents[file][rank][operation]["fs"] = fs	
	pprint.pprint(dic_contents)
	return dic_contents

		
# funcao que calcula o tempo entre as fases de IO que ocorrem na aplicação como um todo
# recebe um lista de tuplas que contem o inico e o fim de cada fase
# retorna uma lista com os valores do tempo dos intervalos de cada fase 
def time_between_operation(list_merge):
	list_result = [] 
	for i in xrange(1, len(list_merge)):
		list_result.append(float(list_merge[i][0]) - float(list_merge[i-1][1]))
	return list_result
	
# funcao que calcula o tempo das fases da IO que ocorrem na aplicação como um todo
# recebe um lista de tuplas que contem o inico e o fim de cada fase
# retorna uma lista com os valores do tempo de cada fase 
def time_operation(list_merge):
	list_result = [] 
	for v_merge in list_merge:
		list_result.append(float(v_merge[1]) - float(v_merge[0]))
	return list_result

# funcao que calcula o tempo de cada fase, levando em consideracao overlap
# recebe o dicionario com os valores lidos do arquivo do darshan, uma lista
# das operacoes desejadas e True ou False para operacoes coletivas
# retorna uma lista tupla com o tempo de inicio e fim de cada operacao
def overlap(dic_contents,operation,collective):
	list_k_file = dic_contents.keys()
	list_v_start = []
	list_v_end =[]
	for k_file in list_k_file:
		list_k_rank = dic_contents[k_file].keys()
		for k_rank in list_k_rank:
			for k_operation in operation:
				if not collective:
					if k_rank == "-1":
					 	continue
					values = dic_contents[k_file][k_rank][k_operation]
					list_v_start.append(values["start"])
					list_v_end.append(values["end"])
				elif collective:
					if k_rank != "-1":
						continue
					values = dic_contents[k_file][k_rank][k_operation]
					list_v_start.append(values["start"])
					list_v_end.append(values["end"])
	list_of_pairs = zip(list_v_start,list_v_end)
	list_merge = merge(list_of_pairs)

# print amigavel 
	print "Operation Blocks"
	count = 0
	for v_merge in list_merge:
		count += 1
		print "Block =",count, "Start =",v_merge[0], "End =",v_merge[1]
	return list_merge

# funcao auxiliar da funcao overlap, realiza o teste de overlep True ou False
# recebe uma lista tupla
# retorna uma lista tupla dos intervalos considerando overlap
def merge(intervals):
    if not intervals:
	    print "PANIC! Not Intervals of Data"
    list_of_pairs = []
    for interval in intervals:
	    list_of_pairs.append((interval[0], 0))
	    list_of_pairs.append((interval[1], 1))
    list_of_pairs.sort() 
    merged = []
    stack = [list_of_pairs[0]]
    for i in xrange(1, len(list_of_pairs)):
	    d = list_of_pairs[i]
	    if d[1] == 0:
	        stack.append(d)
	    elif d[1] == 1:
	        if stack:
		        start = stack.pop()
	        if len(stack) == 0:
		        merged.append( (start[0], d[0]))
    return merged

# #funcao para a visualizacao dos resultados de forma amigavel
def print_results(list_operation,list_between_operation):
	print "Time Operation Blocks"
	count = 0
	for v_operation in list_operation:
		count += 1
		print "Block =",count, "Time =",v_operation
	print "Time Between Operation Blocks"
	count = 0
	for v_operation in list_between_operation:
		count += 1
		print "Block =",count,"at",count+1, "Time =",v_operation

# funcao main 
def main():

	parser = argparse.ArgumentParser(description='Argumentos')
	parser.add_argument('--file', "-f",required=True,help= "Arquivo a ser Parseado")
	parser.add_argument('--operation', "-o",required=True,help= "Para somente leitura: 1; Para somente escrita:2; Para ambas as operações juntas:3")
	parser.add_argument('--collective', "-c",required=True,help= "Para operações coletivas:1; Para operações não coletivas:2; Para ambas operações coletivas:3")
	 
	args = parser.parse_args()
	 
	list_arg_operation = [] 
	collective = False
	collective_all = False
	print("File = {}".format(args.file))
	if(args.operation == "1"):
		print("Operation = Read")
		list_arg_operation = ["read"]
	elif(args.operation == "2"):
		print("Operation = Write")
		list_arg_operation = ["write"]
	elif(args.operation == "3"):
		print("Operation = Overlap Read/Write")
		list_arg_operation = ["read","write"]
	else:
		print("Operation = Invalid value")
		return 0

	if(args.collective == "1"):
		print("Collective = True")
		collective = True
	elif(args.collective == "2"):
		print("Collective = False")
		collective = False
	elif(args.collective == "3"):
		print("Collective = True/False")
		collective_all = True
	else:
		print("Collective = Invalid value")
		return 0

	   
	dic_contents = {}
	dic_header = {}
	list_merge = []
	list_operation =[]
	list_between_operation = []
	counters = ["CP_F_READ_START_TIMESTAMP", "CP_F_READ_END_TIMESTAMP", "CP_F_WRITE_START_TIMESTAMP","CP_F_WRITE_END_TIMESTAMP"]
	filename = args.file

	arq = open(filename,"r")
	dic_header = read_darshan_header(arq)
	dic_contents = read_darshan_file(arq, counters)

	if not collective_all:
		list_merge = overlap(dic_contents, list_arg_operation, collective)
		list_operation = time_operation(list_merge)
		list_between_operation = time_between_operation(list_merge)

		print_results(list_operation,list_between_operation)

	else:
		print "Results of Collective True"
		list_merge = overlap(dic_contents, list_arg_operation, True)
		list_operation = time_operation(list_merge)
		list_between_operation = time_between_operation(list_merge)
		
		print_results(list_operation,list_between_operation)
		print "\n"
		print "Results of Collective False"
		list_merge = overlap(dic_contents, list_arg_operation, False)
		list_operation = time_operation(list_merge)
		list_between_operation = time_between_operation(list_merge)
		
		print_results(list_operation,list_between_operation)

	print "\n"
	print "Execution Completed Successfully"
	return 0


if __name__ == "__main__":
    main()