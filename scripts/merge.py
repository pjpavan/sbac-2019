import pandas as pd
import numpy as np
import collections

final = pd.read_csv("month-10/final.csv", sep=',', index_col=0)
representative = pd.read_csv("month-10/representative.csv", sep=',', index_col=0)

phases = list(final)

phases.remove('file')
phases.remove('runtime')

for f_index, f_row in final.iterrows():
    for r_index, r_row in representative.iterrows():
        if (f_row['file'] == r_row['file']):
            for ph in phases:
                if (r_row['phases'] == ph):
                    final.loc[f_index, ph]  = r_row['total_time']/1000*100/f_row['runtime']
                    
final.to_csv("month-10/results_merge.csv", sep=',', encoding='utf-8')
