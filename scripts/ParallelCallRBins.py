import multiprocessing as np
import sys
import os
import os.path
import argparse
import time
import glob
import shlex
import shutil
import datetime
import subprocess
import logging
import logging.handlers


def worker(filename):


	logger.info('started - R script: {}'.format(filename)) 
	try:
		callR(filename)
	except:
		logger.error('error - R script: {}'.format(filename))
		return 0
	logger.info('end - R script: {}'.format(filename)) 
	

def callR(filename):
	#stdout=DEVNULL,  stderr=subprocess.STDOUT,
	DEVNULL = open(os.devnull, 'wb')
	subprocess.call("Rscript --vanilla intervals.R "+filename+"",  shell=True)

def configureLog():
		# Creates and configures the looger
		logger.setLevel(logging.DEBUG)

		# Set the format
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(processName)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler("parallelcallr.log", maxBytes=268435456, backupCount=50, encoding='utf8')

		# Set the format to the log handler
		handler.setFormatter(formatter)

		# Se the configuration for the log
		logger.addHandler(handler)

		logger.info('Parallel Call R has started')

def main():
	
	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--filespath', "-f",required=True,help= "Files' path")
	parser.add_argument('--process', "-p",required=True,help= "Process Numbers")

	 
	args = parser.parse_args()

	configureLog()

	#subprocess.call("Rscript --save load_librarys.R", shell=True)

	jobs = []
	list_paths = []
	for path in glob.iglob(str(args.filespath)+'/*/*.parsed-interval.csv'):
		logger.info('found: {}'.format(path))
		list_paths.append(path)


	with np.Pool(processes=int(args.process)) as pool:
		for i in range(len(list_paths)):
			pool.map(worker, [list_paths[i],])

	return 0

if __name__ == "__main__":
	logger = logging.getLogger('ParallelCallR')
	main()


