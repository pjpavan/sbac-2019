import os
import sys
import zlib
import json
import csv
import time
import glob
import shlex
import shutil
import datetime
import subprocess
import logging
import logging.handlers
import threading
import collections

from queue import Queue
from threading import Thread

if sys.version_info[0] < 3:
    print('You must use Python 3')
    exit()

class ParallelAccessSize:

	LOG_FILENAME = 'parallel-information.log'

	# Construtor
	def __init__(self, n):
		self.configureLog()

		self.logger.info('ParallelAccessSize has started')

		# Initialize a thread poll with the desired number of threads to update the rank of each problem
		self.pool = ThreadPool(n)

		# Create a mutex
		self.CSVMutex = threading.Lock()

		self.csvfile = open('information.csv', 'w')
		fieldnames = ['jobid','uid', 'exec', 'nprocs', 'runtime', 'total_io_time', 'read_posix_0_100', 'read_posix_100_1K', 'read_posix_1K_10K', 'read_posix_10K_100K', 'read_posix_100K_1M', 'read_posix_1M_4M', 'read_posix_4M_10M', 'read_posix_10M_100M', 'read_posix_100M_1G', 'read_posix_1G_PLUS', 'read_mpi_0_100', 'read_mpi_100_1K', 'read_mpi_1K_10K', 'read_mpi_10K_100K', 'read_mpi_100K_1M', 'read_mpi_1M_4M', 'read_mpi_4M_10M', 'read_mpi_10M_100M', 'read_mpi_100M_1G', 'read_mpi_1G_PLUS', 'write_posix_0_100', 'write_posix_100_1K', 'write_posix_1K_10K', 'write_posix_10K_100K', 'write_posix_100K_1M', 'write_posix_1M_4M', 'write_posix_4M_10M', 'write_posix_10M_100M', 'write_posix_100M_1G', 'write_posix_1G_PLUS', 'write_mpi_0_100', 'write_mpi_100_1K', 'write_mpi_1K_10K', 'write_mpi_10K_100K', 'write_mpi_100K_1M', 'write_mpi_1M_4M', 'write_mpi_4M_10M', 'write_mpi_10M_100M', 'write_mpi_100M_1G', 'write_mpi_1G_PLUS', 'count_posix_reads', 'count_posix_writes', 'count_fstream_reads', 'count_fstream_writes', 'count_nc_mpi_reads', 'count_nc_mpi_writes', 'count_c_mpi_reads', 'count_c_mpi_writes', 'count_s_mpi_reads', 'count_s_mpi_writes', 'count_nb_mpi_reads', 'count_nb_mpi_writes', 'darshan_version', 'original_path']
		#fieldnames = ['jobid', 'exec', 'nprocs', 'runtime', 'total_io_time', 'total_bytes', 'total_bytes_read', 'total_bytes_write', ]
		self.CSVWriter = csv.writer(self.csvfile,  delimiter = ';', quoting = csv.QUOTE_MINIMAL)
		self.CSVWriter.writerow(fieldnames)

	# Configure the log
	def configureLog(self):
		# Creates and configures the looger
		self.logger = logging.getLogger('ParallelAccessSize')
		self.logger.setLevel(logging.DEBUG)

		# Set the format
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(threadName)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler(self.LOG_FILENAME, maxBytes=268435456, backupCount=50, encoding='utf8')

		# Set the format to the log handler
		handler.setFormatter(formatter)

		# Se the configuration for the log
		self.logger.addHandler(handler)


	def run(self):
		year = 2012

		for month in range(1, 13):
			# Reset the dictionary for each month
			self.applications = dict()

			self.logger.info('started to parse {}/{}'.format(year, month))

			for day in range(1, 32):
				full_path = '{}/{}/{}'.format(year, month, day)

				# Check if the folder exisits
				if not os.path.isdir(full_path):
					continue

				# Iterate over all the files, and for each one, create a task
				for path in glob.iglob('{}/*.json'.format(full_path)):
					self.logger.info('found: {}'.format(path))
					self.pool.add_task(self.getInformation, path)

			self.pool.wait_completion()

			self.logger.info('finished to parse {}/{}'.format(year, month))

		self.csvfile.close()


	# Function that gets:
	# |_slowest rank time from unique files and shared files -  shared files time is computed by the slowest time rank counter
	# |_total_bytes_read
	# |_total_bytes_written
	# |_total_bytes_read_by_mpi
	# |_total_bytes_written_by_mpi
	# |_total_bytes_read_by_posix
	# |_total_bytes_written_by_posix
	# |_total_bytes_read_by_stdio
	# |_total_bytes_written_by_stdio
	# creation date: 2018-06-02
	# Author: Andre R. Carneiro
	# in: num_procs,dictionary with the contents
	# out: slowest_unique_time,slowest_shared_time

	# Modification date: 2018-06-26
	# Mod Author: Pablo Jose Pavan
	# rm 'elif (dict_ranks["operations"]["opens"] > 0):' at line 265
	# add else: at line 265

	# Modification date: 2018-08-08
	# Mod Author: Valéria S. Girelli
	# add extraction of access sizes

        # Modification date: 2018-08-28
        # Mod Author: Valéria S. Girelli
        # add extraction of I/O operation counters
	def getInformation(self, path):
		self.logger.info('starting: {}'.format(path))

		dJSON = collections.OrderedDict()
		dJSON = self.openJSON(path)
		dic_header = dJSON["header"]

		#print(dJSON)
		jobid = dic_header["jobid"]
		num_procs = int(dic_header["nprocs"])
		log_version = float(dic_header["darshan"])
		execc = dic_header["exe"]
		runtime = dic_header["run_time"]
		uid = dic_header["uid"]

		slowest_shared_time = 0.0
		bytes_read = 0
		bytes_written = 0
		mpi_bytes_read = 0
		mpi_bytes_written = 0
		posix_bytes_read = 0
		posix_bytes_written = 0
		stdio_bytes_read = 0
		stdio_bytes_written = 0
		mpi_coll_reads= 0
		mpi_indep_reads= 0
		posix_reads= 0
		stdio_reads= 0
		mpi_coll_writes= 0
		mpi_indep_writes= 0
		posix_writes= 0
		stdio_writes= 0

		read_posix_0_100= 0
		read_posix_100_1K= 0
		read_posix_1K_10K= 0
		read_posix_10K_100K= 0
		read_posix_100K_1M= 0
		read_posix_1M_4M= 0
		read_posix_4M_10M= 0
		read_posix_10M_100M= 0
		read_posix_100M_1G= 0
		read_posix_1G_PLUS= 0
		read_mpi_0_100= 0
		read_mpi_100_1K= 0
		read_mpi_1K_10K= 0
		read_mpi_10K_100K= 0
		read_mpi_100K_1M= 0
		read_mpi_1M_4M= 0
		read_mpi_4M_10M= 0
		read_mpi_10M_100M= 0
		read_mpi_100M_1G= 0
		read_mpi_1G_PLUS= 0
		write_posix_0_100= 0
		write_posix_100_1K= 0
		write_posix_1K_10K= 0
		write_posix_10K_100K= 0
		write_posix_100K_1M= 0
		write_posix_1M_4M= 0
		write_posix_4M_10M= 0
		write_posix_10M_100M= 0
		write_posix_100M_1G= 0
		write_posix_1G_PLUS= 0
		write_mpi_0_100= 0
		write_mpi_100_1K= 0
		write_mpi_1K_10K= 0
		write_mpi_10K_100K= 0
		write_mpi_100K_1M= 0
		write_mpi_1M_4M= 0
		write_mpi_4M_10M= 0
		write_mpi_10M_100M= 0
		write_mpi_100M_1G= 0
		write_mpi_1G_PLUS= 0
		count_posix_reads = 0
		count_posix_writes = 0
		count_fstream_reads = 0
		count_fstream_writes = 0
		count_nc_mpi_reads = 0
		count_nc_mpi_writes = 0
		count_c_mpi_reads = 0
		count_c_mpi_writes = 0
		count_s_mpi_reads =0
		count_s_mpi_writes = 0
		count_nb_mpi_reads = 0
		count_nb_mpi_writes = 0

		#list of ranks and its times for the unique files. The list index = rank
		list_rank_uniq_times = [0.0 for x in range(num_procs)]

		#dictionary with the ranks and with files it acessed
		#the structure is: dict_done_files {rank: [list_of_files_accessed], ...}
		#the file stored on the list will have the following preference of interface used by the rank: mpi-io, posix, stdio
		dict_done_files = collections.OrderedDict()
		dict_done_files = {x: [] for x in range(-1,num_procs)}

		#cycle through modules
		#Only processes the modules "mpi-io", "posix", "stdio"
		#Start with mpi-io
	#	for module,dict_modules in dict_contents.items():
		dict_contents = dJSON["data"]
		#print(dict_contents)
		list_module= []

		if (log_version >= 3.0):
			list_module= ["mpi-io", "posix", "stdio"]
		else:
			list_module= ["all"]

		for module in list_module:

			if (not module in dict_contents.keys()):
				continue

			dict_modules= dict_contents[module]

			for file_record,dict_files in dict_modules.items():

				#ignores records on fs unknown or sent to stderr/sdtout
				#on certain gromacs records, there was negative values for counters SLOWEST/FASTEST_RANK_TIME and MODULE_F_READ/WRITE/META_TIME
				if (log_version >= 3.0) and ((dict_files["fs"] == "UNKNOWN") or (dict_files["filename"].replace("<","").replace(">","") in ["STDOUT", "STDERR"] ) ):
				#       ("do nothing")
					continue

                                # items() list of dict's key,value pairs
				for rank_key,dict_ranks in dict_files["ranks"].items():
					#print(dict_ranks["operations"])
					rank = int(rank_key)

					read_posix_0_100 += int(dict_ranks["size"]["read"]["posix_0_100"])
					read_posix_100_1K += int(dict_ranks["size"]["read"]["posix_100_1K"])
					read_posix_1K_10K += int(dict_ranks["size"]["read"]["posix_1K_10K"])
					read_posix_10K_100K += int(dict_ranks["size"]["read"]["posix_10K_100K"])
					read_posix_100K_1M += int(dict_ranks["size"]["read"]["posix_100K_1M"])
					read_posix_1M_4M += int(dict_ranks["size"]["read"]["posix_1M_4M"])
					read_posix_4M_10M += int(dict_ranks["size"]["read"]["posix_4M_10M"])
					read_posix_10M_100M += int(dict_ranks["size"]["read"]["posix_10M_100M"])
					read_posix_100M_1G += int(dict_ranks["size"]["read"]["posix_100M_1G"])
					read_posix_1G_PLUS += int(dict_ranks["size"]["read"]["posix_1G_PLUS"])
					read_mpi_0_100 += int(dict_ranks["size"]["read"]["mpi_0_100"])
					read_mpi_100_1K += int(dict_ranks["size"]["read"]["mpi_100_1K"])
					read_mpi_1K_10K += int(dict_ranks["size"]["read"]["mpi_1K_10K"])
					read_mpi_10K_100K += int(dict_ranks["size"]["read"]["mpi_10K_100K"])
					read_mpi_100K_1M += int(dict_ranks["size"]["read"]["mpi_100K_1M"])
					read_mpi_1M_4M += int(dict_ranks["size"]["read"]["mpi_1M_4M"])
					read_mpi_4M_10M += int(dict_ranks["size"]["read"]["mpi_4M_10M"])
					read_mpi_10M_100M += int(dict_ranks["size"]["read"]["mpi_10M_100M"])
					read_mpi_100M_1G += int(dict_ranks["size"]["read"]["mpi_100M_1G"])
					read_mpi_1G_PLUS += int(dict_ranks["size"]["read"]["mpi_1G_PLUS"])
					write_posix_0_100 += int(dict_ranks["size"]["write"]["posix_0_100"])
					write_posix_100_1K += int(dict_ranks["size"]["write"]["posix_100_1K"])
					write_posix_1K_10K += int(dict_ranks["size"]["write"]["posix_1K_10K"])
					write_posix_10K_100K += int(dict_ranks["size"]["write"]["posix_10K_100K"])
					write_posix_100K_1M += int(dict_ranks["size"]["write"]["posix_100K_1M"])
					write_posix_1M_4M += int(dict_ranks["size"]["write"]["posix_1M_4M"])
					write_posix_4M_10M += int(dict_ranks["size"]["write"]["posix_4M_10M"])
					write_posix_10M_100M += int(dict_ranks["size"]["write"]["posix_10M_100M"])
					write_posix_100M_1G += int(dict_ranks["size"]["write"]["posix_100M_1G"])
					write_posix_1G_PLUS += int(dict_ranks["size"]["write"]["posix_1G_PLUS"])
					write_mpi_0_100 += int(dict_ranks["size"]["write"]["mpi_0_100"])
					write_mpi_100_1K += int(dict_ranks["size"]["write"]["mpi_100_1K"])
					write_mpi_1K_10K += int(dict_ranks["size"]["write"]["mpi_1K_10K"])
					write_mpi_10K_100K += int(dict_ranks["size"]["write"]["mpi_10K_100K"])
					write_mpi_100K_1M += int(dict_ranks["size"]["write"]["mpi_100K_1M"])
					write_mpi_1M_4M += int(dict_ranks["size"]["write"]["mpi_1M_4M"])
					write_mpi_4M_10M += int(dict_ranks["size"]["write"]["mpi_4M_10M"])
					write_mpi_10M_100M += int(dict_ranks["size"]["write"]["mpi_10M_100M"])
					write_mpi_100M_1G += int(dict_ranks["size"]["write"]["mpi_100M_1G"])
					write_mpi_1G_PLUS += int(dict_ranks["size"]["write"]["mpi_1G_PLUS"])
					count_posix_reads += int(dict_ranks["operations"]["reads"])
					count_posix_writes += int(dict_ranks["operations"]["writes"])
					#count_fstream_reads += int(dict_ranks["operations"][""])
					#count_fstream_writes += int(dict_ranks["operations"][""])
					count_nc_mpi_reads += int(dict_ranks["operations"]["indep_reads"])
					count_nc_mpi_writes += int(dict_ranks["operations"]["indep_writes"])
					count_c_mpi_reads += int(dict_ranks["operations"]["coll_reads"])
					count_c_mpi_writes += int(dict_ranks["operations"]["coll_writes"])
					count_s_mpi_reads += int(dict_ranks["operations"]["split_reads"])
					count_s_mpi_writes += int(dict_ranks["operations"]["split_writes"])
					count_nb_mpi_reads += int(dict_ranks["operations"]["nb_reads"])
					count_nb_mpi_writes += int(dict_ranks["operations"]["nb_writes"])

					#first verify if the rank time has already been computed for this file
					if (file_record in set(dict_done_files[rank])):
					#	("do nothing")
						continue

					time_sum = 0.0
					#sums the time spent on read, write and meta opeartion
					#verify with wich module the file was opened
					if (module == "mpi-io"):
						if (dict_ranks["operations"]["indep_opens"] > 0 or dict_ranks["operations"]["coll_opens"] > 0):
						#	("file opened with mpi-io")
							#TIME
							#verify if it's a unique or shared operation. rank = -1 -> shared
							if (rank == -1):
								slowest_shared_time+= float(dict_ranks["performance"]["slowest_rank_time"])
							else:
								#sum the time_operations on the time of the rank for unique files
								time_sum = dict_ranks["time"]["mpi_read"] + dict_ranks["time"]["mpi_write"] + dict_ranks["time"]["mpi_meta"]
								list_rank_uniq_times[rank] += time_sum

							#BYTES
							bytes_written += int(dict_ranks["bytes"]["write"])
							mpi_bytes_written += int(dict_ranks["bytes"]["write"])
							bytes_read += int(dict_ranks["bytes"]["read"])
							mpi_bytes_read += int(dict_ranks["bytes"]["read"])
							
							#COUNTS
							mpi_coll_reads+= int(dict_ranks["operations"]["coll_reads"])
							mpi_indep_reads+= int(dict_ranks["operations"]["indep_reads"])
							mpi_coll_writes+= int(dict_ranks["operations"]["coll_writes"])
							mpi_indep_writes+= int(dict_ranks["operations"]["indep_writes"])

							#update the rank list of used files
							dict_done_files[rank].append(file_record)

					elif (module == "posix"):
						if (dict_ranks["operations"]["opens"] > 0):
						#	("file opened with posix")

							#verify if it's a unique or shared operation. rank = -1 -> shared
							if (rank == -1):
								slowest_shared_time += float(dict_ranks["performance"]["slowest_rank_time"])
							else:
								#sum the time_operations on the time of the rank for unique files
								time_sum= dict_ranks["time"]["posix_read"] + dict_ranks["time"]["posix_write"] + dict_ranks["time"]["posix_meta"]
								list_rank_uniq_times[rank] += time_sum

							#BYTES
							bytes_written+= int(dict_ranks["bytes"]["write"])
							posix_bytes_written+= int(dict_ranks["bytes"]["write"])
							bytes_read+= int(dict_ranks["bytes"]["read"])
							posix_bytes_read+= int(dict_ranks["bytes"]["read"])
			
							#COUNTS
							posix_reads+= int(dict_ranks["operations"]["reads"])
							posix_writes+= int(dict_ranks["operations"]["writes"])


							#update the rank list of used files
							dict_done_files[rank].append(file_record)

					elif (module == "stdio"):
						if (dict_ranks["operations"]["opens"] > 0):
						#	("file opened with stdio")
							#verify if it's a unique or shared operation. rank = -1 -> shared
							if (rank == -1):
								slowest_shared_time+= float(dict_ranks["performance"]["slowest_rank_time"])
							else:
								#sum the time_operations on the time of the rank for unique files
								time_sum= dict_ranks["time"]["stdio_read"]+dict_ranks["time"]["stdio_write"]+dict_ranks["time"]["stdio_meta"]
								list_rank_uniq_times[rank]+= time_sum

							#BYTES
							bytes_written += int(dict_ranks["bytes"]["write"])
							stdio_bytes_written += int(dict_ranks["bytes"]["write"])
							bytes_read += int(dict_ranks["bytes"]["read"])
							stdio_bytes_read += int(dict_ranks["bytes"]["read"])
			
							#COUNTS
							stdio_reads+= int(dict_ranks["operations"]["reads"])
							stdio_writes+= int(dict_ranks["operations"]["writes"])
		
							#update the rank list of used files
							dict_done_files[rank].append(file_record)

					#module == all -> old darshan log version
					elif (module == "all"):
						#verify with wich module the file was opened
						if (dict_ranks["operations"]["indep_opens"] > 0 or dict_ranks["operations"]["coll_opens"] > 0):
						#	("file opened with mpi-io")
							#verify if it's a unique or shared operation. rank = -1 -> shared
							if (rank == -1):
								try:
									slowest_shared_time += float(dict_ranks["performance"]["slowest_rank_time"])
								except:
									self.logger.error('not found slowest_rank_time: {}'.format(path))
									slowest_shared_time +=0	
							else:
								#sum the time_operations on the time of the rank for unique files
								time_sum= dict_ranks["time"]["mpi_read"] + dict_ranks["time"]["mpi_write"] + dict_ranks["time"]["mpi_meta"]
								list_rank_uniq_times[rank]+= time_sum
							#BYTES
							bytes_written += int(dict_ranks["bytes"]["write"])
							mpi_bytes_written += int(dict_ranks["bytes"]["write"])
							bytes_read += int(dict_ranks["bytes"]["read"])
							mpi_bytes_read += int(dict_ranks["bytes"]["read"])
			
							#COUNTS
							mpi_coll_reads+= int(dict_ranks["operations"]["coll_reads"])
							mpi_indep_reads+= int(dict_ranks["operations"]["indep_reads"])
							mpi_coll_writes+= int(dict_ranks["operations"]["coll_writes"])
							mpi_indep_writes+= int(dict_ranks["operations"]["indep_writes"])
		
							#update the rank list of used files
							dict_done_files[rank].append(file_record)

						#file opened with posix
						else:
							#verify if it's a unique or shared operation. rank = -1 -> shared
							if (rank == -1):
								try:
									slowest_shared_time += float(dict_ranks["performance"]["slowest_rank_time"])
								except:
									self.logger.error('not found slowest_rank_time: {}'.format(path))
									slowest_shared_time +=0	
							else:
								#sum the time_operations on the time of the rank for unique files
								time_sum= dict_ranks["time"]["posix_read"] + dict_ranks["time"]["posix_write"] + dict_ranks["time"]["posix_meta"]
								list_rank_uniq_times[rank] += time_sum
							#BYTES
							bytes_written += int(dict_ranks["bytes"]["write"])
							stdio_bytes_written += int(dict_ranks["bytes"]["write"])
							bytes_read += int(dict_ranks["bytes"]["read"])
							stdio_bytes_read += int(dict_ranks["bytes"]["read"])

							try:
								stdio_reads+= int(dict_ranks["operations"]["reads"]) + int(dict_ranks["operations"]["freads"])
								stdio_writes+= int(dict_ranks["operations"]["writes"]) + int(dict_ranks["operations"]["fwrites"])
							except:
								stdio_reads+= int(dict_ranks["operations"]["reads"])
								stdio_writes+= int(dict_ranks["operations"]["writes"])

							#update the rank list of used files
							dict_done_files[rank].append(file_record)

		#sort the list of unique file times
		list_rank_uniq_times.sort()

		row = [
			jobid,
			uid,
			execc,
			num_procs,
			runtime,
			list_rank_uniq_times[len(list_rank_uniq_times)-1] + slowest_shared_time,
			read_posix_0_100,
			read_posix_100_1K,
			read_posix_1K_10K,
			read_posix_10K_100K,
			read_posix_100K_1M,
			read_posix_1M_4M,
			read_posix_4M_10M,
			read_posix_10M_100M,
			read_posix_100M_1G,
			read_posix_1G_PLUS,
			read_mpi_0_100,
			read_mpi_100_1K,
			read_mpi_1K_10K,
			read_mpi_10K_100K,
			read_mpi_100K_1M,
			read_mpi_1M_4M,
			read_mpi_4M_10M,
			read_mpi_10M_100M,
			read_mpi_100M_1G,
			read_mpi_1G_PLUS,
			write_posix_0_100,
			write_posix_100_1K,
			write_posix_1K_10K,
			write_posix_10K_100K,
			write_posix_100K_1M,
			write_posix_1M_4M,
			write_posix_4M_10M,
			write_posix_10M_100M,
			write_posix_100M_1G,
			write_posix_1G_PLUS,
			write_mpi_0_100,
			write_mpi_100_1K,
			write_mpi_1K_10K,
			write_mpi_10K_100K,
			write_mpi_100K_1M,
			write_mpi_1M_4M,
			write_mpi_4M_10M,
			write_mpi_10M_100M,
			write_mpi_100M_1G,
			write_mpi_1G_PLUS,
			count_posix_reads,
			count_posix_writes,
			#count_fstream_reads,
			#count_fstream_writes,
			count_nc_mpi_reads,
			count_nc_mpi_writes,
			count_c_mpi_reads,
			count_c_mpi_writes,
			count_s_mpi_reads,
			count_s_mpi_writes,
			count_nb_mpi_reads,
			count_nb_mpi_writes,
			log_version,
			path
		]

		with self.CSVMutex:
			self.CSVWriter.writerow(row)

	def openJSON(self, filename):
		dJSON = collections.OrderedDict()

		with open(filename, 'rb') as fileJSON:
			data = fileJSON.read()

			try:
				# Try to read compressed JSON file
				decompressed = zlib.decompress(data)
				dJSONdata = json.loads(str(decompressed, 'utf-8'))
				dJSON= json.loads(dJSONdata)
			except:
				try:
					# Try to read JSON file
					dJSON = json.loads(data)
				except:
					self.logger.error('unable to open: {}'.format(filename))
					return {}

		return dJSON


class Worker(Thread):
	""" Thread executing tasks from a given tasks queue """
	def __init__(self, tasks):
		Thread.__init__(self)
		self.tasks = tasks
		self.daemon = True
		self.start()

	def run(self):
		while True:
			func, args, kargs = self.tasks.get()
			try:
				func(*args, **kargs)
			except Exception as e:
				# An exception happened in this thread
				print(type(e))
				print(e.args)
				print(e)
				#sys.exit()
			finally:
				# Mark this task as done, whether an exception happened or not
				self.tasks.task_done()


class ThreadPool:
	""" Pool of threads consuming tasks from a queue """
	def __init__(self, num_threads):
		self.tasks = Queue(num_threads)
		for _ in range(num_threads):
			Worker(self.tasks)

	def add_task(self, func, *args, **kargs):
		""" Add a task to the queue """
		self.tasks.put((func, args, kargs))

	def map(self, func, args_list):
		""" Add a list of tasks to the queue """
		for args in args_list:
			self.add_task(func, args)

	def wait_completion(self):
		""" Wait for completion of all the tasks in the queue """
		self.tasks.join()
