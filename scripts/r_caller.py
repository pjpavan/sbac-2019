import glob         # used for find the pathnames matching a specified pattern
import os           # used for OS dependent functionalities
import sys
import subprocess   # used for create new processes, connect them with pipes, and get their return value
import zlib
import json
import collections

rscript = "./phases_collects.R"

if sys.version_info[0] < 3:
    print('You must use Python 3')
    exit()

def open_feather():
    # # seraching for olam's feathers.
    # for path in glob.iglob('./**/*olam*.parsed_statistics.feather', recursive = True):
    #     subprocess.run("Rscript " + rscript + " " + path + "", shell = True)
    #     #print (path)

    # seraching for gromacs's feathers.
    for path in glob.iglob('data/*.parsed_statistics.feather', recursive=True):
        subprocess.run("Rscript " + rscript + " " + path + "", shell = True)
        #print (path)

open_feather()

