import multiprocessing as np
import sys
import os
import os.path
import pprint
import json
import argparse
import collections
import mmap
import zlib
import csv
import time
import glob
import shlex
import shutil
import datetime
import subprocess
import logging
import logging.handlers

def open_json(filename):
	dJSON = collections.OrderedDict()
	with open(filename, 'rb') as fileJSON:
			data = fileJSON.read()

			try:
				# Try to read compressed JSON file
				decompressed = zlib.decompress(data)
				dJSONdata = json.loads(str(decompressed, 'utf-8'))
				dJSON= json.loads(dJSONdata)
			except:
				try:
					# Try to read JSON file
					dJSON = json.loads(data)
				except:
					self.logger.error('unable to open: {}'.format(filename))
					return {}
	return dJSON


# Function that gets the list of all timestamps
# creation date: 2018-07-03
# Author: Andre R. Carneiro
# in: dictionary with the contents
# out: lists with the start and end timestamps, and the original id of the record: [(start, end, id), (start, end, id), ....]
# out: dictionary with the information about the interval

def get_timestamps(dict_header, dict_contents):
	timestamps_read= []
	timestamps_write= []
	timestamps= []
	dict_timestamps_info= collections.OrderedDict()
	num_procs= int(dict_header["nprocs"])
	log_version= float(dict_header["darshan"])

	#dictionary with the ranks and with files it acessed
	#the structure is: dict_done_files {rank: [list_of_files_accessed], ...}
	#the file stored on the list will have the following preference of interface used by the rank: mpi-io, posix, stdio
	dict_done_files= collections.OrderedDict()
	dict_done_files= {x: [] for x in range(-1,num_procs)}


	#cycle through modules
	#Only processes the modules "mpi-io", "posix", "stdio"
	#Start with mpi-io
	list_module= []
	if (log_version >= 3.0):
		list_module= ["mpi-io", "posix", "stdio"]
	else:
		list_module= ["all"]

	for module in list_module:

		if (not module in dict_contents.keys()):
			continue

		dict_modules= dict_contents[module]
		for file_record,dict_files in dict_modules.items():
			#ignores records on fs unknown or sent to stderr/sdtout
			#necessery because some gromacs logs had negative values
			if (dict_files["filename"].replace("<","").replace(">","") in ["STDOUT", "STDERR"] ):
				continue
			for rank_key,dict_ranks in dict_files["ranks"].items():
				rank=int(rank_key)
				#first verify if the rank time has already been computed for this file
				if (file_record in set(dict_done_files[rank])):
				#       ("do nothing")
					continue

				#verify if the timestamps are negative
				if (dict_ranks["timestamp"]["read"]["start"] < 0) or (dict_ranks["timestamp"]["read"]["end"] < 0) or (dict_ranks["timestamp"]["write"]["start"] < 0) or (dict_ranks["timestamp"]["write"]["end"] < 0):
				#       ("do nothing")
					continue

				if (module == "mpi-io"):
					#verifi if there was a read
					if (int(dict_ranks["operations"]["coll_reads"]) > 0) or (int(dict_ranks["operations"]["indep_reads"]) > 0):
						#record the read timestamp
						current_id=len(timestamps)


						timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"],current_id))
						

						#create the dectionary to store info about the timestamp
						info=module+" read"

						#verifi if it was a collective or independet operation
						if (int(dict_ranks["operations"]["coll_reads"]) > 0):
							info+=" collective"
						if (int(dict_ranks["operations"]["indep_reads"]) > 0):
							info+=" independent"

						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"

						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes
						#ID_access_size = the size of the four most common access sizes
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

					#verifi if there was a write
					if (int(dict_ranks["operations"]["coll_writes"]) > 0) or (int(dict_ranks["operations"]["indep_writes"]) > 0):
						current_id=len(timestamps)
						timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"],current_id))


						#create the dectionary to store info about the timestamp
						info=module+" write"

						#verifi if it was a collective or independet operation
						if (int(dict_ranks["operations"]["coll_reads"]) > 0):
							info+=" collective"
						if (int(dict_ranks["operations"]["indep_reads"]) > 0):
							info+=" independent"

						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"
						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes
						#ID_access_size = the size of the four most common access sizes
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

				elif (module == "posix"):
					#verifi if there was a read
					if (int(dict_ranks["operations"]["reads"]) > 0):
						#record the read timestamp
						current_id=len(timestamps)
						timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"], current_id))


						#create the dectionary to store info about the timestamp
						info=module+" read"

						#verifi if it was a consecutive / sequential operation
						if (int(dict_ranks["operations"]["consec_writes"]) > 0):
							info+=" consecutive"
						if (int(dict_ranks["operations"]["seq_writes"]) > 0):
							info+=" sequential"

						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"
						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes
						#ID_access_size = the size of the four most common access sizes
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

					#verifi if there was a write
					if (int(dict_ranks["operations"]["writes"]) > 0):
						current_id=len(timestamps)
						timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"], current_id))

						#create the dectionary to store info about the timestamp
						info=module+" write"

						#verifi if it was a consecutive / sequential operation
						if (int(dict_ranks["operations"]["consec_writes"]) > 0):
							info+=" consecutive"
						if (int(dict_ranks["operations"]["seq_writes"]) > 0):
							info+=" sequential"


						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"
						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes
						#ID_access_size = the size of the four most common access sizes
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

				elif (module == "stdio"):
					#verifi if there was a read
					if (int(dict_ranks["operations"]["reads"]) > 0):
						#record the read timestamp
						current_id=len(timestamps)
						timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"], current_id))

						#create the dectionary to store info about the timestamp
						info=module+" read"

						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"
						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes - stdio has no access counter
						#ID_access_size = the size of the four most common access sizes - stdio has no access counter
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': 0, '1_access_size': 0, '2_access_count': 0, '2_access_size': 0, '3_access_count': 0, '3_access_size': 0, '4_access_count': 0, '4_access_size': 0}

					#verifi if there was a write
					if (int(dict_ranks["operations"]["writes"]) > 0):
						current_id=len(timestamps)
						timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"], current_id))


						#create the dectionary to store info about the timestamp
						info=module+" write"

						#verifi if it was a shared file (rank -1) or unique file
						if (rank == -1):
							info+=" shared file"
						else:
							info+=" unique file"
						#update the rank list of used files
						dict_done_files[rank].append(file_record)

						#record the interval info on the dictionary
						#info = information about the operation
						#file = the file of the interval
						#ID_access_count = the count of the four most common access sizes - stdio has no access counter
						#ID_access_size = the size of the four most common access sizes - stdio has no access counter
						dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': 0, '1_access_size': 0, '2_access_count': 0, '2_access_size': 0, '3_access_count': 0, '3_access_size': 0, '4_access_count': 0, '4_access_size': 0}

				elif (module == "all"):
					#verify if it was a mpi operation 
					if (int(dict_ranks["operations"]["coll_opens"]) > 0) or (int(dict_ranks["operations"]["indep_opens"]) > 0):
						#verify if there was a read
						if (int(dict_ranks["operations"]["coll_reads"]) > 0) or (int(dict_ranks["operations"]["indep_reads"]) > 0):
							#record the read timestamp
							current_id=len(timestamps)
							timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"],current_id))
							
							#create the dectionary to store info about the timestamp
							info="mpi-io read"
	
							#verifi if it was a collective or independet operation
							if (int(dict_ranks["operations"]["coll_reads"]) > 0):
								info+=" collective"
							if (int(dict_ranks["operations"]["indep_reads"]) > 0):
								info+=" independent"
	
							#verifi if it was a shared file (rank -1) or unique file
							if (rank == -1):
								info+=" shared file"
							else:
								info+=" unique file"
	
							#update the rank list of used files
							dict_done_files[rank].append(file_record)

							#record the interval info on the dictionary
							#info = information about the operation
							#file = the file of the interval
							#ID_access_count = the count of the four most common access sizes
							#ID_access_size = the size of the four most common access sizes
							dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

						#verifi if there was a write
						if (int(dict_ranks["operations"]["coll_writes"]) > 0) or (int(dict_ranks["operations"]["indep_writes"]) > 0):
							current_id=len(timestamps)
							timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"],current_id))
	
	
							#create the dectionary to store info about the timestamp
							info="mpi-io write"
	
							#verifi if it was a collective or independet operation
							if (int(dict_ranks["operations"]["coll_reads"]) > 0):
								info+=" collective"
							if (int(dict_ranks["operations"]["indep_reads"]) > 0):
								info+=" independent"
	
							#verifi if it was a shared file (rank -1) or unique file
							if (rank == -1):
								info+=" shared file"
							else:
								info+=" unique file"
							#update the rank list of used files
							dict_done_files[rank].append(file_record)

							#record the interval info on the dictionary
							#info = information about the operation
							#file = the file of the interval
							#ID_access_count = the count of the four most common access sizes
							#ID_access_size = the size of the four most common access sizes
							dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

					#verify if it was a posix operation
					if (int(dict_ranks["operations"]["opens"]) > 0):
						#verifi if there was a read
						if (int(dict_ranks["operations"]["reads"]) > 0):
							#record the read timestamp
							current_id=len(timestamps)
							timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"], current_id))

							#create the dectionary to store info about the timestamp
							info="posix read"
	
							#verifi if it was a consecutive / sequential operation
							if (int(dict_ranks["operations"]["consec_writes"]) > 0):
								info+=" consecutive"
							if (int(dict_ranks["operations"]["seq_writes"]) > 0):
								info+=" sequential"
	
							#verifi if it was a shared file (rank -1) or unique file
							if (rank == -1):
								info+=" shared file"
							else:
								info+=" unique file"
							#update the rank list of used files
							dict_done_files[rank].append(file_record)	

							#record the interval info on the dictionary
							#info = information about the operation
							#file = the file of the interval
							#ID_access_count = the count of the four most common access sizes
							#ID_access_size = the size of the four most common access sizes
							dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

						#verifi if there was a write
						if (int(dict_ranks["operations"]["writes"]) > 0):
							current_id=len(timestamps)
							timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"], current_id))
	
							#create the dectionary to store info about the timestamp
							info="posix write"
	
							#verifi if it was a consecutive / sequential operation
							if (int(dict_ranks["operations"]["consec_writes"]) > 0):
								info+=" consecutive"
							if (int(dict_ranks["operations"]["seq_writes"]) > 0):
								info+=" sequential"
	
	
							#verifi if it was a shared file (rank -1) or unique file
							if (rank == -1):
								info+=" shared file"
							else:
								info+=" unique file"
		
							#update the rank list of used files
							dict_done_files[rank].append(file_record)

							#record the interval info on the dictionary
							#info = information about the operation
							#file = the file of the interval
							#ID_access_count = the count of the four most common access sizes
							#ID_access_size = the size of the four most common access sizes
							dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}

					#verify if it was a posix foperation
					if ("fopens" in dict_ranks["operations"].keys()):
						if (int(dict_ranks["operations"]["fopens"]) > 0):
							#verifi if there was a read
							if (int(dict_ranks["operations"]["freads"]) > 0):
								#record the read timestamp
								current_id=len(timestamps)
								timestamps.append((dict_ranks["timestamp"]["read"]["start"], dict_ranks["timestamp"]["read"]["end"], current_id))
	
								#create the dectionary to store info about the timestamp
								info="posix fread"
		
								#verifi if it was a consecutive / sequential operation
								if (int(dict_ranks["operations"]["consec_writes"]) > 0):
									info+=" consecutive"
								if (int(dict_ranks["operations"]["seq_writes"]) > 0):
									info+=" sequential"
		
								#verifi if it was a shared file (rank -1) or unique file
								if (rank == -1):
									info+=" shared file"
								else:
									info+=" unique file"
								#update the rank list of used files
								dict_done_files[rank].append(file_record)	
	
								#record the interval info on the dictionary
								#info = information about the operation
								#file = the file of the interval
								#ID_access_count = the count of the four most common access sizes
								#ID_access_size = the size of the four most common access sizes
								dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}
	
							#verifi if there was a write
							if (int(dict_ranks["operations"]["fwrites"]) > 0):
								current_id=len(timestamps)
								timestamps.append((dict_ranks["timestamp"]["write"]["start"], dict_ranks["timestamp"]["write"]["end"], current_id))
	
								#create the dectionary to store info about the timestamp
								info="posix fwrite"
		
								#verifi if it was a consecutive / sequential operation
								if (int(dict_ranks["operations"]["consec_writes"]) > 0):
									info+=" consecutive"
								if (int(dict_ranks["operations"]["seq_writes"]) > 0):
									info+=" sequential"
	
	
								#verifi if it was a shared file (rank -1) or unique file
								if (rank == -1):
									info+=" shared file"
								else:
									info+=" unique file"
		
								#update the rank list of used files
								dict_done_files[rank].append(file_record)
	
								#record the interval info on the dictionary
								#info = information about the operation
								#file = the file of the interval
								#ID_access_count = the count of the four most common access sizes
								#ID_access_size = the size of the four most common access sizes
								dict_timestamps_info[current_id]= {'info': info, 'file': file_record, '1_access_count': dict_ranks["accesses"]["1"]["count"], '1_access_size': dict_ranks["accesses"]["1"]["access"], '2_access_count': dict_ranks["accesses"]["2"]["count"], '2_access_size': dict_ranks["accesses"]["2"]["access"], '3_access_count': dict_ranks["accesses"]["3"]["count"], '3_access_size': dict_ranks["accesses"]["3"]["access"], '4_access_count': dict_ranks["accesses"]["4"]["count"], '4_access_size': dict_ranks["accesses"]["4"]["access"]}
								
	return timestamps, dict_timestamps_info






# Function that generate a dictionary with the list of the timestamps 
# This dictionary is later used to write the csv file
# creation date: 2018-07-04
# Author: Andre R. Carneiro
# in: list of start and end timestamps: [(start, end), (start, end), ....] , dictionary with info about each timestamp interval
# out: dictionary to write the csv file
#json_file_id
#	|_'jobid', 'uid', 'runtime', 'exec', 'file', 'interval-id', 'start', 'end', 'info', '1_access_count', '1_access_size', '2_access_count', '2_access_size', '3_access_count', '3_access_size', '4_access_count', '4_access_size'

def get_ts(dict_header,list_ts,dict_ts_info):
	dict_phase = collections.OrderedDict()
	#verify the timestamp list
	if not list_ts:
		return 0

	#sort the timestamp list by the start
	list_sorted= sorted(list_ts, key=lambda ts: ts[0])
	for i in range(0,len(list_sorted)):
		start=  list_sorted[i][0]
		end=  list_sorted[i][1]
		orig_id= list_sorted[i][2]
		aux=i+1

		dict_phase[i]= {'jobid': dict_header["jobid"], 'uid': dict_header["uid"], 'runtime': dict_header["run_time"], 'exec': dict_header["exe"], 'file': dict_ts_info[orig_id]["file"], 'interval-id': aux, 'start': start, 'end': end, 'info': dict_ts_info[orig_id]["info"], '1_access_count': dict_ts_info[orig_id]["1_access_count"], '1_access_size': dict_ts_info[orig_id]["1_access_size"], '2_access_count': dict_ts_info[orig_id]["2_access_count"], '2_access_size': dict_ts_info[orig_id]["2_access_size"], '3_access_count': dict_ts_info[orig_id]["3_access_count"], '3_access_size': dict_ts_info[orig_id]["3_access_size"], '4_access_count': dict_ts_info[orig_id]["4_access_count"], '4_access_size': dict_ts_info[orig_id]["4_access_size"], }


	return dict_phase


# Function that create the csv output file
# creation date: 2018-07-04
# Author: Andre R. Carneiro
# in: dictionary with the gathered data from the json files
# The dictionary have the following format

#json_file_id
#	|_'jobid', 'uid', 'runtime', 'exec', 'file', 'interval-id', 'start', 'end', 'info', '1_access_count', '1_access_size', '2_access_count', '2_access_size', '3_access_count', '3_access_size', '4_access_count', '4_access_size'

def create_csv(output_filename):
	with open(output_filename, 'w') as csvfile:
		fieldnames = ['jobid', 'uid', 'runtime', 'exec', 'file', 'interval-id', 'start', 'end', 'info', 'X1_access_count', 'X1_access_size', 'X2_access_count', 'X2_access_size', 'X3_access_count', 'X3_access_size', 'X4_access_count', 'X4_access_size']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=';')
		writer.writeheader()
	return 0


# Function that writes the csv output file
# creation date: 2018-07-04
# Author: Andre R. Carneiro
# in: dictionary with the gathered data from the json files
# The dictionary have the following format

#json_file_id
#	|_'exec': , 'phase': , 'start': ,'end': , 'info': 

def write_csv(output_filename,dict_contents):
	with open(output_filename, 'a') as csvfile:
		fieldnames = ['jobid', 'uid', 'runtime', 'exec', 'file', 'interval-id', 'start', 'end', 'info','X1_access_count', 'X1_access_size', 'X2_access_count', 'X2_access_size', 'X3_access_count', 'X3_access_size', 'X4_access_count', 'X4_access_size']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=';')
		for keys,values in dict_contents.items():
			writer.writerow(values)

	return 0


def worker(list_paths):

	dict_contents = collections.OrderedDict()
	dict_header = collections.OrderedDict()
	dict_counters = collections.OrderedDict()
	json_file_id= 0

	filename = list_paths
	output_filename =  filename.replace(".json", ".csv")

	#create the output file
	create_csv(output_filename)

	#Dictionary with the contents to save on the csv file
	dict_to_csv = collections.OrderedDict()

	dict_json = collections.OrderedDict()
	dict_json= open_json(filename)

	#Verify if was able to open the json file
	if (not dict_json):
		logger.error('Error while opening {}. Skiping'.format(filename))
		return 0 
		

	#Verify if the json file have the header and data structure
	if not ("data" in dict_json.keys() and "header" in dict_json.keys()):
		logger.error('File {} does not have the header/data structure. Skiping'.format(filename))
		return 0

	#Verify if the log version - v 1.x not supported
	if (float(dict_json["header"]["darshan"]) < 2):
		logger.error('Darshan log version {} not supported. Skiping'.format(dict_json["header"]["darshan"]))
		return 0

	logger.info('start - parser to csv: {}'.format(filename))
	try:
		list_ts, dict_ts = get_timestamps(dict_json["header"], dict_json["data"])
		dict_to_csv= get_ts(dict_json["header"],list_ts,dict_ts)
	except:
		logger.error('error - parser to csv: {}'.format(filename))
	try:	
		if dict_to_csv:
			write_csv(output_filename,dict_to_csv)
	except:
		logger.error('error - write to csv: {}'.format(filename))
	logger.info('end - parser to csv: {}'.format(filename))

	logger.info('start - R script: {}'.format(filename))
	try:
		callR(output_filename)
	except:
		logger.error('error - R script: {}'.format(filename))
		return 0
	logger.info('end - R script: {}'.format(filename)) 
	# try:
	# 	#callrm(output_filename)
	# 	pass:
	# except:
	# 	logger.error('error - rm csv file : {}'.format(filename))
	# 	return 0 
	


def callrm(csv_filename):
	os.remove(csv_filename)


def callR(csv_filename):
	#stdout=DEVNULL,  stderr=subprocess.STDOUT,
	DEVNULL = open(os.devnull, 'wb')
	subprocess.call("Rscript --vanilla intervals.R "+csv_filename+"",  shell=True)

def configureLog():
		# Creates and configures the looger
		logger.setLevel(logging.DEBUG)

		# Set the format
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(processName)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler("intervals.log", maxBytes=268435456, backupCount=50, encoding='utf8')

		# Set the format to the log handler
		handler.setFormatter(formatter)

		# Se the configuration for the log
		logger.addHandler(handler)

		logger.info('Parallel Intervals has started')

def main():
	
	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--filespath', "-f",required=True,help= "Files' path")
	parser.add_argument('--process', "-p",required=True,help= "Process Numbers")

	 
	args = parser.parse_args()


	configureLog()


	subprocess.call("Rscript --save load_librarys.R", shell=True)


	jobs = []
	list_paths = []
	for path in glob.iglob(str(args.filespath)+'/*.json'):
		logger.info('found: {}'.format(path))
		list_paths.append(path)



	with np.Pool(processes=int(args.process)) as pool:
		for i in range(len(list_paths)):
			pool.map(worker, [list_paths[i],])

	return 0

if __name__ == "__main__":
	logger = logging.getLogger('ParallelParser')
	main()


