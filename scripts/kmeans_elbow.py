# clustering dataset
# determine k using elbow method
import pandas as pd
from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
import matplotlib.pyplot as plt
import sys

df = pd.read_csv("month-10/results_merge.csv", sep=',')

df = df.drop(df.columns[[0,1,2]], axis=1)

numpy_matrix = df.as_matrix()

# k means determine k
distortions = []
#fechado, aberto
K = range(int(sys.argv[1]),int(sys.argv[2]))
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(numpy_matrix)
    #kmeanModel.fit(numpy_matrix)
    distortions.append(sum(np.min(cdist(numpy_matrix, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / numpy_matrix.shape[0])

labels = ['k','d']
kd = [list(a) for a in zip(K, distortions)]

df_kd = pd.DataFrame.from_records(kd , columns=labels)

df_kd.to_csv("month-10/elbow_"+sys.argv[1]+"_"+sys.argv[2]+"_phases.csv", sep=',', encoding='utf-8')

