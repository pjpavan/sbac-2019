import os
import sys
import zlib
import json
import csv
import time
import glob
import shlex
import shutil
import datetime
import subprocess
import logging
import logging.handlers
import threading
import collections

from queue import Queue
from threading import Thread

if sys.version_info[0] < 3:
    print('You must use Python 3')
    exit()

class ParallelInformer:

	LOG_FILENAME = 'parallel-information.log'

	# Construtor
	def __init__(self, n):
		self.configureLog()

		self.logger.info('ParallelInformer has started')

		# Initialize a thread poll with the desired number of threads to update the rank of each problem
		self.pool = ThreadPool(n)

		# Create a mutex
		self.CSVMutex = threading.Lock()

		self.csvfile = open('information.csv', 'w')
		fieldnames = ['jobid','uid', 'exec', 'nprocs', 'runtime', 'r_0_100', 'r_100_1K', 'r_1K_10K', 'r_10K_100K', 'r_100K_1M', 'r_1M_4M', 'r_4M_10M', 'r_10M_100M', 'r_100M_1G', 'r_1G_PLUS', 'r_AGG_0_100', 'r_AGG_100_1K', 'r_AGG_1K_10K', 'r_AGG_10K_100K', 'r_AGG_100K_1M', 'r_AGG_1M_4M', 'r_AGG_4M_10M', 'r_AGG_10M_100M', 'r_AGG_100M_1G', 'r_AGG_1G_PLUS', 'w_0_100', 'w_100_1K', 'w_1K_10K', 'w_10K_100K', 'w_100K_1M', 'w_1M_4M', 'w_4M_10M', 'w_10M_100M', 'w_100M_1G', 'w_1G_PLUS', 'w_AGG_0_100', 'w_AGG_100_1K', 'w_AGG_1K_10K', 'w_AGG_10K_100K', 'w_AGG_100K_1M', 'w_AGG_1M_4M', 'w_AGG_4M_10M', 'w_AGG_10M_100M', 'w_AGG_100M_1G', 'w_AGG_1G_PLUS']
		#fieldnames = ['jobid', 'exec', 'nprocs', 'runtime', 'total_io_time', 'total_bytes', 'total_bytes_read', 'total_bytes_write', ]
		self.CSVWriter = csv.writer(self.csvfile,  delimiter = ';', quoting = csv.QUOTE_MINIMAL)
		self.CSVWriter.writerow(fieldnames)

	# Configure the log
	def configureLog(self):
		# Creates and configures the looger
		self.logger = logging.getLogger('ParallelInformer')
		self.logger.setLevel(logging.DEBUG)

		# Set the format
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(threadName)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler(self.LOG_FILENAME, maxBytes=268435456, backupCount=50, encoding='utf8')

		# Set the format to the log handler
		handler.setFormatter(formatter)

		# Se the configuration for the log
		self.logger.addHandler(handler)


	def run(self):
		year = 2012

		for month in range(1, 13):
			# Reset the dictionary for each month
			self.applications = dict()

			self.logger.info('started to parse {}/{}'.format(year, month))

			for day in range(1, 32):
				full_path = '{}/{}/{}'.format(year, month, day)

				# Check if the folder exisits
				if not os.path.isdir(full_path):
					continue

				# Iterate over all the files, and for each one, create a task
				for path in glob.iglob('{}/*.json'.format(full_path)):
					self.logger.info('found: {}'.format(path))

					self.pool.add_task(self.getInformation, path)

			self.pool.wait_completion()

			self.logger.info('finished to parse {}/{}'.format(year, month))

		self.csvfile.close()


	# Function that gets:
	# |_slowest rank time from unique files and shared files -  shared files time is computed by the slowest time rank counter
	# |_total_bytes_read
	# creation date: 2018-08-06
        # Author: Valéria S. Girelli
	# in: path to the file
	# out: 

	def getInformation(self, path):
		self.logger.info('starting: {}'.format(path))

		dJSON = collections.OrderedDict()
		dJSON = self.openJSON(path)
		dic_header = dJSON["header"]

		#print(dJSON)
		jobid = dic_header["jobid"]
		num_procs = int(dic_header["nprocs"])
		log_version = float(dic_header["darshan"])
		execc = dic_header["exe"]
		runtime = dic_header["run_time"]
                uid = dic_header["uid"]

		slowest_shared_time = 0.0
		bytes_read = 0
		bytes_written = 0
		mpi_bytes_read = 0
		mpi_bytes_written = 0
		posix_bytes_read = 0
		posix_bytes_written = 0
		stdio_bytes_read = 0
		stdio_bytes_written = 0
		mpi_coll_reads= 0
		mpi_indep_reads= 0
		posix_reads= 0
		stdio_reads= 0
		mpi_coll_writes= 0
		mpi_indep_writes= 0
		posix_writes= 0
		stdio_writes= 0

		#dictionary with the ranks and with files it acessed
		#the structure is: dict_done_files {rank: [list_of_files_accessed], ...}
		#the file stored on the list will have the following preference of interface used by the rank: mpi-io, posix, stdio
		dict_done_files = collections.OrderedDict()
		dict_done_files = {x: [] for x in range(-1,num_procs)}

		#cycle through modules
		#Only processes the modules "mpi-io", "posix", "stdio"
		#Start with mpi-io
	#	for module,dict_modules in dict_contents.items():
		dict_contents = dJSON["data"]
		list_module= []

		if (log_version >= 3.0):
			list_module= ["mpi-io", "posix", "stdio"]
		else:
			list_module= ["all"]

		for module in list_module:

			if (not module in dict_contents.keys()):
				continue

			dict_modules= dict_contents[module]

# TODO VAL
			for file_record,dict_files in dict_modules.items():

				#ignores records on fs unknown or sent to stderr/sdtout
				#on certain gromacs records, there was negative values for counters SLOWEST/FASTEST_RANK_TIME and MODULE_F_READ/WRITE/META_TIME
				if (log_version >= 3.0) and ((dict_files["fs"] == "UNKNOWN") or (dict_files["filename"].replace("<","").replace(">","") in ["STDOUT", "STDERR"] ) ):
				#       ("do nothing")
					continue

                                # items() list of dict's key,value pairs
				for rank_key,dict_ranks in dict_files["ranks"].items():
					rank = int(rank_key)
					
					#first verify if the rank time has already been computed for this file
					if (file_record in set(dict_done_files[rank])):
					#	("do nothing")
						continue

					time_sum = 0.0
					#sums the time spent on read, write and meta opeartion
					#verify with wich module the file was opened
					if (module == "mpi-io"):

					elif (module == "posix"):

					elif (module == "stdio"):

					#module == all -> old darshan log version
					elif (module == "all"):

		row = [
			jobid,
                        uid,
			execc,
			num_procs,
			runtime,
                        r_0_100,
                        r_100_1K,
                        r_1K_10K,
                        r_10K_100K,
                        r_100K_1M,
                        r_1M_4M, r_4M_10M,
                        r_10M_100M,
                        r_100M_1G,
                        r_1G_PLUS,
                        r_AGG_0_100,
                        r_AGG_100_1K,
                        r_AGG_1K_10K,
                        r_AGG_10K_100K,
                        r_AGG_100K_1M,
                        r_AGG_1M_4M,
                        r_AGG_4M_10M,
                        r_AGG_10M_100M,
                        r_AGG_100M_1G,
                        r_AGG_1G_PLUS,
                        w_0_100,
                        w_100_1K,
                        w_1K_10K,
                        w_10K_100K,
                        w_100K_1M,
                        w_1M_4M,
                        w_4M_10M,
                        w_10M_100M,
                        w_100M_1G,
                        w_1G_PLUS,
                        w_AGG_0_100,
                        w_AGG_100_1K,
                        w_AGG_1K_10K,
                        w_AGG_10K_100K,
                        w_AGG_100K_1M,
                        w_AGG_1M_4M,
                        w_AGG_4M_10M,
                        w_AGG_10M_100M,
                        w_AGG_100M_1G,
                        w_AGG_1G_PLUS
		]

		with self.CSVMutex:
			self.CSVWriter.writerow(row)

	def openJSON(self, filename):
		dJSON = collections.OrderedDict()

		with open(filename, 'rb') as fileJSON:
			data = fileJSON.read()

			try:
				# Try to read compressed JSON file
				decompressed = zlib.decompress(data)
				dJSONdata = json.loads(str(decompressed, 'utf-8'))
				dJSON= json.loads(dJSONdata)
			except:
				try:
					# Try to read JSON file
					dJSON = json.loads(data)
				except:
					self.logger.error('unable to open: {}'.format(filename))
					return {}

		return dJSON


class Worker(Thread):
	""" Thread executing tasks from a given tasks queue """
	def __init__(self, tasks):
		Thread.__init__(self)
		self.tasks = tasks
		self.daemon = True
		self.start()

	def run(self):
		while True:
			func, args, kargs = self.tasks.get()
			try:
				func(*args, **kargs)
			except Exception as e:
				# An exception happened in this thread
				print(e)
			finally:
				# Mark this task as done, whether an exception happened or not
				self.tasks.task_done()


class ThreadPool:
	""" Pool of threads consuming tasks from a queue """
	def __init__(self, num_threads):
		self.tasks = Queue(num_threads)
		for _ in range(num_threads):
			Worker(self.tasks)

	def add_task(self, func, *args, **kargs):
		""" Add a task to the queue """
		self.tasks.put((func, args, kargs))

	def map(self, func, args_list):
		""" Add a list of tasks to the queue """
		for args in args_list:
			self.add_task(func, args)

	def wait_completion(self):
		""" Wait for completion of all the tasks in the queue """
		self.tasks.join()
