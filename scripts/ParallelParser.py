import os
import sys
import time
import glob
import shlex
import shutil
import datetime
import subprocess
import logging
import logging.handlers

from queue import Queue
from threading import Thread

class ParallelParser:

	EXECUTION = 'parser_mod.py'
	LOG_FILENAME = 'parallel-parser.log'

	# Construtor
	def __init__(self, n):
		self.configureLog()

		self.logger.info('ParallelParser has started')

		# Initialize a thread poll with the desired number of threads to update the rank of each problem
		self.pool = ThreadPool(n)

	# Configure the log
	def configureLog(self):
		# Creates and configures the looger
		self.logger = logging.getLogger('ParallelParser')
		self.logger.setLevel(logging.DEBUG)

		# Set the format
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(threadName)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler(self.LOG_FILENAME, maxBytes=268435456, backupCount=50, encoding='utf8')

		# Set the format to the log handler
		handler.setFormatter(formatter)

		# Se the configuration for the log
		self.logger.addHandler(handler)

	def run(self):
		# Iterate over all the files, and for each one, create a task
		for path in glob.iglob('data/*.bz2'):
			self.logger.info('found: {}'.format(path))
			#name_json = path.replace(".bz2", ".parsed.json")
			#if not (os.path.isfile(name_json)):
			self.pool.add_task(self.generateJSON, path)

		self.pool.wait_completion()

	def generateJSON(self, path):
		self.logger.info('starting: {}'.format(path))

		name = os.path.splitext(path)[0]

		with open('{}.parsed'.format(name), mode = 'w') as outfile, open('{}.error'.format(name), mode = 'w') as errfile:
			try:
				self.logger.info('parsing: {}'.format(path))

				# Prepare the command
				command = 'darshan-parser {}'.format(path)

				# Execute
				arguments = shlex.split(command)

				d = subprocess.Popen(arguments, stdout = outfile, stderr = errfile)
				output, error = d.communicate()
			except:
				self.logger.error('failed to parse the darshan file: {}'.format(path))

		try:
			self.logger.info('collecting: {}'.format(path))

			# Prepare the command
			command = 'python {} -f {}.parsed'.format(self.EXECUTION, name)

			# Execute
			arguments = shlex.split(command)

			p = subprocess.Popen(arguments)
			output, error = p.communicate()
		except:
			self.logger.error('failed to generate the JSON for the darshan file: {}'.format(path))

		self.logger.info('removing: {}.parsed'.format(name))

		# Removed the parsed file
		os.remove('{}.parsed'.format(name))
		# Removed the error file
		if (os.path.getsize('{}.error'.format(name))) < 2:
			os.remove('{}.error'.format(name))


class Worker(Thread):
    """ Thread executing tasks from a given tasks queue """
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)
            except Exception as e:
                # An exception happened in this thread
                print(e)
            finally:
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()


class ThreadPool:
    """ Pool of threads consuming tasks from a queue """
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads):
            Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """ Add a task to the queue """
        self.tasks.put((func, args, kargs))

    def map(self, func, args_list):
        """ Add a list of tasks to the queue """
        for args in args_list:
            self.add_task(func, args)

    def wait_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()

