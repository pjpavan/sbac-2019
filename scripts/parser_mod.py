﻿import sys
import commands
import os
import os.path
import pprint
import json
import argparse
import collections
import mmap
import zlib

# Modification date: 2018-05-22
# Mod Author: Pablo José Pavan
# 1.4 version add

# Modification date: 2018-06-7
# Mod Author: Pablo José Pavan
# 2.00 version add
LOG_VERSION_1_23 = 1.23
LOG_VERSION_1_24 = 1.24
LOG_VERSION_2_00 = 2.00
LOG_VERSION_2_05 = 2.05
LOG_VERSION_3_10 = 3.10
SUPORTED_LOG_VERIONS= [LOG_VERSION_1_23, LOG_VERSION_1_24,LOG_VERSION_2_00,LOG_VERSION_2_05, LOG_VERSION_3_10]

# Function that extracts data from the header
# of the already parsed Darshan's log file.
# Receives the file as parameter,
# and returns a dictionary with the extracted header:
#
#name
#	|_values
#
# Modification Date: 2018-05-04
# Author: Andre R. Carneiro
# Use "with open ... as" seaches through the file instead of reading line by line
# Receives the filename as parameter,
# Now the file remains open only when needed

def read_darshan_header(filename):
	darshan_log_ver = 0.0
	exe = None
	uid = -1
	jobid = 0
	start_time = 0
	end_time = 0
	nprocs = 0
	with open (filename, 'rt') as file_parsed:
		file_map= mmap.mmap(file_parsed.fileno(), 0, access=mmap.ACCESS_READ)

		#search for darshan log version
		index = file_map.find('# darshan log version',0)
		if index != -1:
			file_map.seek(index)
			darshan_log_ver = float(file_map.readline().split(':')[1].strip())

			if not (float(darshan_log_ver) in SUPORTED_LOG_VERIONS):
					print "Unsupported darshan log version\n"
					return 1

		#search for exec name
		index = file_map.find('# exe',0)
		if index != -1:
			file_map.seek(index)
			exe = os.path.basename(file_map.readline().split()[2].strip())

		#search for uid
		index = file_map.find('# uid',0)
		if index != -1:
			file_map.seek(index)
			uid = int(file_map.readline().split(':')[1].strip())
		

		#search for jobid
		# Depending on the darshan log version,
			# there are differences in the structure of the header
		# LOG_VERSION_1_23 and 2_05 -> jobid may come as '# metadata: jobid ='
		# LOG_VERSION_3_10 -> jobid come as '# jobid:'
		index = file_map.find('# jobid',0)
		if index != -1:
			file_map.seek(index)
			jobid = int(file_map.readline().split(':')[1].strip())
		if jobid == 0:
			index = file_map.find('# metadata: jobid',0)
			if index != -1:
				file_map.seek(index)
				jobid = int(file_map.readline().split('=')[1].strip())

		
		#search for start_time
		index = file_map.find('# start_time:',0)
		if index != -1:
			file_map.seek(index)
			start_time = file_map.readline().split(':')[1].strip()

		#search for end_time
		index = file_map.find('# end_time:',0)
		if index != -1:
			file_map.seek(index)
			end_time = file_map.readline().split(':')[1].strip()

		#search for nprocs
		index = file_map.find('# nprocs:',0)
		if index != -1:
			file_map.seek(index)
			nprocs = int(file_map.readline().split(':')[1].strip())

		#search for run time
		index = file_map.find('# run time:',0)
		if index != -1:
			file_map.seek(index)
			run_time = file_map.readline().split(':')[1].strip()

		#close file map
		file_map.close()

		name = "header"
		dict_header = collections.OrderedDict()
		dict_header[name] = collections.OrderedDict()
		dict_header[name]['darshan'] = darshan_log_ver
		dict_header[name]['exe'] = exe
		dict_header[name]['uid'] = uid
		dict_header[name]['jobid'] = jobid
		dict_header[name]['start_time'] = start_time
		dict_header[name]['end_time'] = end_time
		dict_header[name]['nprocs'] = nprocs
		dict_header[name]['run_time'] = run_time
		return dict_header


# function that reads the counter contents from darshan log file
# Input paramter: filename, darshan log verion and a dictionary with the counters to capture, with the following structure:
#module [all | posix,mpi-io,stdio,lustre]
#	|_counter_type_time
#		|_read
#		|_write
#	|_counter_type_operations
#		|_list of operations counters
#	|_counter_type_size
#		|_list of read size
#		|_list of write size
#	|_counter_type_access
#		|_list of access counters

# Output: dictionary with the values:
#name
#	|_module
#		|_file_system
#		|_filename
#       	|_file_record <hash>
#			|_ranks
#				|_rank id
#					|_timestamp
#						|_read
#							|_start
#							|_end
#						|_write
#							|_start
#							|_end
#						|_open (mpi-io doesn't have start/end. Just one value)
#							|_start
#							|_end
#						|_close (mpi-io doesn't have start/end. Just one value)
#							|_start
#							|_end
#					|_time
#						|_read
#						|_write
#						|_meta
#					|_bytes
#						|_read
#						|_write
#					|_operations
#					|_performance
#					|_size
#						|_read
#							|_list
#						|_write
#							|_list
#					|_accesses
#						|_1
#							|_count
#							|_access
#						|_2
#							|_count
#							|_access
#						|_3
#							|_count
#							|_access
#						|_4
#							|_count
#							|_access
# Modification Date: 2018-05-06
# Author: Andre R. Carneiro
# Use "with open ... as" seaches through the file instead of reading line by line
# Receives the filename, log_version and dict_counters as parameter,
# Now the file remains open only when needed

# Modification Date: 2018-05-31
# Author: Andre R. Carneiro
# grabs the filename reported by darshan

# Modification Date: 2018-06-02
# Author: Andre R. Carneiro
# grabs the open/close timestamps and the performance counter (slowest/fastest)

def read_darshan_file_new(filename, dict_counters):
	dict_contents = collections.OrderedDict()
	name = "data"
	dict_contents[name] = collections.OrderedDict()

	with open (filename, 'rt') as file_parsed:
		file_map= mmap.mmap(file_parsed.fileno(), 0, access=mmap.ACCESS_READ)
		for module,list_counters in dict_counters.items():
			#create a module name in uppercase, removing the - from MPI-IO
			#this is used later on, to remove the MODULENAME_ prefix from the counter field name
			module_prefix= module.replace("-","").upper()

			#go to the beginig of each module record
			#record format: <module>\t<rank>...
			index = file_map.find(module.upper()+"\t",0)
			if index == -1:
				#print("The following module is not present: "+module)
				continue

			dict_contents[name][module] = collections.OrderedDict()	

			file_map.seek(index)
			line= file_map.readline()
			
			while (module.upper() in line):
				if len(line) == 0:
					continue
				#reads the records with the following format: 
				#<module>	<rank>	<record id>	<counter>	<value>	<file name>	<mount pt>	<fs type>
				null, rank, file, counter, value, file_name, null, fs= line.split()
				if not file in dict_contents[name][module]:
					dict_contents[name][module][file] = collections.OrderedDict()
					dict_contents[name][module][file]['ranks'] = collections.OrderedDict()
					dict_contents[name][module][file]['fs'] = fs
					dict_contents[name][module][file]['filename'] = file_name

				#create the structure list of counters
				if not rank in dict_contents[name][module][file]['ranks']:
					dict_contents[name][module][file]['ranks'][rank] = collections.OrderedDict()
					#TIMESTAMP counters
					counter_type= "timestamp"
					if (list_counters[counter_type]["read"] and list_counters[counter_type]["write"]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["read"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["write"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["open"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["close"]= collections.OrderedDict()
					#TIME counters
					counter_type= "time"
					if (list_counters[counter_type]["read"] and list_counters[counter_type]["write"]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
					#BYTES counters
					counter_type= "bytes"
					if (list_counters[counter_type]["read"] and list_counters[counter_type]["write"]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
					#OPERATIONS counters
					counter_type= "operations"
					if (list_counters[counter_type]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
					#PERFORMANCE counters
					counter_type= "performance"
					if (list_counters[counter_type]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
					#SIZE counters
					counter_type= "size"
					if (list_counters[counter_type]["read"] and list_counters[counter_type]["write"]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["read"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["write"]= collections.OrderedDict()
					#ACCESSES counters
					counter_type= "accesses"
					if (list_counters[counter_type]):
						dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["1"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["2"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["3"]= collections.OrderedDict()
						dict_contents[name][module][file]['ranks'][rank][counter_type]["4"]= collections.OrderedDict()

	
				#Store the information
				#TIMESTAMP read counters
				if counter in list_counters["timestamp"]["read"]:
					if "START" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['read']['start'] = float(value)
					elif "END" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['read']['end'] = float(value)
				#TIMESTAMP write counters
				elif counter in list_counters["timestamp"]["write"]:
					if "START" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['write']['start'] = float(value)
					elif "END" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['write']['end'] = float(value)
				#TIMESTAMP open counters
				if counter in list_counters["timestamp"]["open"]:
					if "START" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['open']['start'] = float(value)
					elif "END" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['open']['end'] = float(value)
					else:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['open']['start'] = float(value)
				#TIMESTAMP close counters
				elif counter in list_counters["timestamp"]["close"]:
					if "START" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['close']['start'] = float(value)
					elif "END" in counter:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['close']['end'] = float(value)
					else:
						dict_contents[name][module][file]['ranks'][rank]['timestamp']['close']['end'] = float(value)
				#TIME read counters
				elif counter in list_counters["time"]["read"]:
					dict_contents[name][module][file]['ranks'][rank]['time'][module.replace("-io","")+'_read'] = float(value)
				#TIME write counters
				elif counter in list_counters["time"]["write"]:
					dict_contents[name][module][file]['ranks'][rank]['time'][module.replace("-io","")+'_write'] = float(value)
				#TIME meta counters
				elif counter in list_counters["time"]["meta"]:
					dict_contents[name][module][file]['ranks'][rank]['time'][module.replace("-io","")+'_meta'] = float(value)
				#BYTES read counters
				elif counter in list_counters["bytes"]["read"]:
					dict_contents[name][module][file]['ranks'][rank]['bytes']['read'] = value
				#BYTES write counters
				elif counter in list_counters["bytes"]["write"]:
					dict_contents[name][module][file]['ranks'][rank]['bytes']['write'] = value
				#OPERATIONS counters
				elif counter in list_counters["operations"]:
					#remove the prefix "MODULENAME_" from the counter field.
					new_counter_name= counter.replace(module_prefix+"_","")
					if not new_counter_name.lower() in dict_contents[name][module][file]['ranks'][rank]["operations"]:
						dict_contents[name][module][file]['ranks'][rank]["operations"][new_counter_name.lower()] = int(value)
				#PERFORMANCE counters
				elif counter in list_counters["performance"]:
					#remove the prefix "MODULENAME_" from the counter field.
					new_counter_name= counter.replace(module_prefix+"_","").replace("F_","")
					if not new_counter_name.lower() in dict_contents[name][module][file]['ranks'][rank]["performance"]:
						dict_contents[name][module][file]['ranks'][rank]["performance"][new_counter_name.lower()] = value
				#SIZE read counters
				elif counter in list_counters["size"]["read"]:
					#remove the prefix "MODULENAME_" from the counter field.
					#aditionally remove the AGG from the MPI-IO module counter field name
					new_counter_name= counter.replace(module_prefix+"_SIZE_READ_","").replace("AGG_","")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["read"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["read"][module.replace("-io","")+'_'+new_counter_name] = int(value)	 
				#SIZE write counters
				elif counter in list_counters["size"]["write"]:
					#remove the prefix "MODULENAME_" from the counter field.
					#aditionally remove the AGG from the MPI-IO module counter field name
					new_counter_name= counter.replace(module_prefix+"_SIZE_WRITE_","").replace("AGG_","")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["write"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["write"][module.replace("-io","")+'_'+new_counter_name] = int(value)	 
				#ACCESS counters
				elif counter in list_counters["accesses"]:
					#remove the prefix "MODULENAME_" from the counter field.
					new_counter_name= counter.replace(module_prefix+"_ACCESS","")
					#get the access id and field type (access or count)
					access_id, access_type= new_counter_name.lower().split("_")
					if not access_type in dict_contents[name][module][file]['ranks'][rank]["accesses"][access_id]:
						dict_contents[name][module][file]['ranks'][rank]["accesses"][access_id][access_type] = int(value)
				
				line= file_map.readline()
		#close file map
		file_map.close()
	return dict_contents

# Modification Date: 2018-05-22
# Author: Pablo José Pavan

# Modification Date: 2018-06-02
# Author: Andre R. Carneiro
# grabs the open/close timestamps and the performance counter (slowest/fastest)

def read_darshan_file_old(filename, dict_counters):
	dict_contents = collections.OrderedDict()
	with open (filename, 'rt') as file_parsed:
		file_map= mmap.mmap(file_parsed.fileno(), 0, access=mmap.ACCESS_READ)
		name = "data"
		module = "all"
		dict_contents[name] = collections.OrderedDict()
		dict_contents[name][module] = collections.OrderedDict()

		#Advances to the begning of the records
		index = file_map.find("#<rank>",0)
		if index == -1:
			#print("There's no record. Exiting")
			return dict_contents
		file_map.seek(index)
		line = file_map.readline()
		while (line):
			line = file_map.readline()
			if len(line) == 0:
				continue
			rank, file, counter, value, file_name, null, fs = line.split()
			if not file in dict_contents[name][module]:
				dict_contents[name][module][file] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'] = collections.OrderedDict()
				dict_contents[name][module][file]['fs'] = fs
				dict_contents[name][module][file]['filename'] = file_name
			#create the structure list of counters
			if not rank in dict_contents[name][module][file]['ranks']:
				dict_contents[name][module][file]['ranks'][rank] = collections.OrderedDict()
				#TIME counters
				counter_type= "timestamp"
				dict_contents[name][module][file]['ranks'][rank][counter_type] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["read"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["write"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["open"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["close"] = collections.OrderedDict()
				#TIME counters
				counter_type= "time"
				dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()			
				#BYTES counters
				counter_type= "bytes"
				dict_contents[name][module][file]['ranks'][rank][counter_type]= collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["read"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["write"] = collections.OrderedDict()
				#OPERATIONS counters
				counter_type= "operations"
				dict_contents[name][module][file]['ranks'][rank][counter_type] = collections.OrderedDict()
				#PERFORMANCE counters
				counter_type= "performance"
				dict_contents[name][module][file]['ranks'][rank][counter_type] = collections.OrderedDict()
				#SIZE counters
				counter_type= "size"
				dict_contents[name][module][file]['ranks'][rank][counter_type] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["read"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["write"] = collections.OrderedDict()
				#ACCESSES counters
				counter_type= "accesses"
				dict_contents[name][module][file]['ranks'][rank][counter_type] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["1"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["2"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["3"] = collections.OrderedDict()
				dict_contents[name][module][file]['ranks'][rank][counter_type]["4"] = collections.OrderedDict()
	
			#TIMESTAMP read counters
			if counter in dict_counters['timestamp']['read']:
				if "START" in counter:
					dict_contents[name][module][file]['ranks'][rank]['timestamp']['read']['start'] = float(value)
				elif "END" in counter:
					dict_contents[name][module][file]['ranks'][rank]['timestamp']['read']['end'] = float(value)
			#TIMESTAMP write counters
			elif counter in dict_counters['timestamp']['write']:
				if "START" in counter:
					dict_contents[name][module][file]['ranks'][rank]['timestamp']['write']['start'] = float(value)
				elif "END" in counter:
					dict_contents[name][module][file]['ranks'][rank]['timestamp']['write']['end'] = float(value)
			#TIMESTAMP open counters
			elif counter in dict_counters['timestamp']['open']:
				dict_contents[name][module][file]['ranks'][rank]['timestamp']['open']['start'] = float(value)
			#TIMESTAMP close counters
			elif counter in dict_counters['timestamp']['close']:
				dict_contents[name][module][file]['ranks'][rank]['timestamp']['close']['start'] = float(value)
			#TIME read counters
			elif counter in dict_counters['time']['read']:
				if "POSIX" in counter:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
				else:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
			#TIME write counters
			elif counter in dict_counters['time']['write']:
				if "POSIX" in counter:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
				else:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
			#TIME meta counters
			elif counter in dict_counters['time']['meta']:
				if "POSIX" in counter:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
				else:
					new_counter_name= counter.replace("CP_F_","")
					new_counter_name= new_counter_name.replace("_TIME","")
					dict_contents[name][module][file]['ranks'][rank]['time'][new_counter_name.lower()] = float(value)
			#BYTES read counters
			elif counter in dict_counters['bytes']['read']:
				dict_contents[name][module][file]['ranks'][rank]['bytes']['read'] = value
			#BYTES write counters
			elif counter in dict_counters['bytes']['write']:
				dict_contents[name][module][file]['ranks'][rank]['bytes']['write'] = value
			#OPERATIONS counters
			elif counter in dict_counters['operations']:
				#remove the prefix "CP_" from the counter field.
				#Also removes the POSIX_ from the fields
				new_counter_name= counter.replace("CP_","").replace("POSIX_","")
				if not new_counter_name.lower() in dict_contents[name][module][file]['ranks'][rank]["operations"]:
					dict_contents[name][module][file]['ranks'][rank]["operations"][new_counter_name.lower()] = int(value)
			#OPERATIONS counters
			elif counter in dict_counters['performance']:
				#remove the prefix "CP_" from the counter field.
				#Also removes the F_ from the fields
				new_counter_name= counter.replace("CP_","").replace("F_","")
				if not new_counter_name.lower() in dict_contents[name][module][file]['ranks'][rank]["performance"]:
					dict_contents[name][module][file]['ranks'][rank]["performance"][new_counter_name.lower()] = value
			#SIZE read counters
			elif counter in dict_counters['size']['read']:
				#remove the prefix "CP_" from the counter field.
				#Also removes the AGG_ from the fields, in case of MPI-IO
				if "AGG" in counter:
					new_counter_name= counter.replace("CP_SIZE_READ_AGG","mpi")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["read"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["read"][new_counter_name] = int(value)
				else:
					new_counter_name= counter.replace("CP_SIZE_READ","posix")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["read"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["read"][new_counter_name] = int(value)
			#SIZE write counters
			elif counter in dict_counters['size']['write']:
				#remove the prefix "CP_" from the counter field.
				#Also removes the AGG_ from the fields, in case of MPI-IO
				if "AGG" in counter:
					new_counter_name= counter.replace("CP_SIZE_WRITE_AGG","mpi")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["write"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["write"][new_counter_name] = int(value)
				else:
					new_counter_name= counter.replace("CP_SIZE_WRITE","posix")
					if not new_counter_name in dict_contents[name][module][file]['ranks'][rank]["size"]["write"]:
						dict_contents[name][module][file]['ranks'][rank]["size"]["write"][new_counter_name] = int(value)	
			#ACCESS counters
			elif counter in dict_counters['accesses']:
				#remove the prefix "CP_" from the counter field.
				new_counter_name= counter.replace("CP_ACCESS","")
				#get the access id and field type (access or count)
				access_id, access_type= new_counter_name.lower().split("_")
				if not access_type in dict_contents[name][module][file]['ranks'][rank]["accesses"][access_id]:
					dict_contents[name][module][file]['ranks'][rank]["accesses"][access_id][access_type] = int(value)	 	
		#Close the file
		file_map.close()
	return dict_contents


# Function that creates the counters dictionary strucutre based on the darshan log version
# in: darshan log version
# out: counters dictionary, with the following structure
#module [all | posix,mpi-io,stdio,lustre]
#	|_counter_type_time
#		|_read
#		|_write
#	|_counter_type_timestamp
#		|_read
#		|_write
#		|_open
#		|_close
#	|_counter_type_operations
#		|_list of operations counters
#	|_counter_type_performance
#		|_list of performance counters
#	|_counter_type_size
#		|_list of read size
#		|_list of write size
#	|_counter_type_access
#		|_list of access counters

# Modification Date: 2018-06-22
# Author: Pablo José Pavan
# News counters add: fopens, freads, fwrites, fseeks

def create_counter_structure(log_version):
	dict_counters= collections.OrderedDict()

###########################LOG_VERSION_ 1.23 and 2.05#######################
	if log_version < LOG_VERSION_3_10:	
		#Define the module "all" dictionary
		module = "all"
		dict_counters[module]= collections.OrderedDict()

		#define the timestamp read counters
		dict_counters[module]["timestamp"]= collections.OrderedDict()
		dict_counters[module]["timestamp"]["read"]= ["CP_F_READ_START_TIMESTAMP",  "CP_F_READ_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["write"]= ["CP_F_WRITE_START_TIMESTAMP", "CP_F_WRITE_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["open"]= ["CP_F_OPEN_TIMESTAMP"]
		#define the timestamp write counters
		dict_counters[module]["timestamp"]["close"]= ["CP_F_CLOSE_TIMESTAMP"]

		#define the time read counters
		dict_counters[module]["time"]= collections.OrderedDict()
		dict_counters[module]["time"]["read"]= ["CP_F_POSIX_READ_TIME",  "CP_F_MPI_READ_TIME"]

		#define the time write counters
		dict_counters[module]["time"]["write"]= ["CP_F_POSIX_WRITE_TIME", "CP_F_MPI_WRITE_TIME"]

		#define the time write counters
		dict_counters[module]["time"]["meta"]= ["CP_F_POSIX_META_TIME", "CP_F_MPI_META_TIME"]

		#define the bytes read counters
		dict_counters[module]["bytes"]= collections.OrderedDict()
		dict_counters[module]["bytes"]["read"]= ["CP_BYTES_READ"]

		#define the timestamp write counters
		dict_counters[module]["bytes"]["write"]= ["CP_BYTES_WRITTEN"]

		#define the performance counters
		dict_counters[module]["performance"] = ["CP_FASTEST_RANK", "CP_FASTEST_RANK_BYTES", "CP_SLOWEST_RANK", "CP_SLOWEST_RANK_BYTES", "CP_F_FASTEST_RANK_TIME", "CP_F_SLOWEST_RANK_TIME"]

		#define the operations counters
		dict_counters[module]["operations"] = ["CP_POSIX_OPENS", "CP_POSIX_READS", "CP_POSIX_WRITES", "CP_POSIX_SEEKS", "CP_POSIX_FOPENS", "CP_POSIX_FREADS", "CP_POSIX_FWRITES", "CP_POSIX_FSEEKS", "CP_INDEP_OPENS", "CP_INDEP_READS", "CP_INDEP_WRITES", "CP_COLL_OPENS", "CP_COLL_READS", "CP_COLL_WRITES", "CP_SPLIT_READS", "CP_SPLIT_WRITES", "CP_NB_READS", "CP_NB_WRITES", "CP_SYNCS", "CP_CONSEC_READS", "CP_CONSEC_WRITES", "CP_SEQ_READS", "CP_SEQ_WRITES"]
		#define the size read counters
		dict_counters[module]["size"]= collections.OrderedDict()
		dict_counters[module]["size"]["read"] = ["CP_SIZE_READ_0_100",  "CP_SIZE_READ_100_1K",  "CP_SIZE_READ_1K_10K",  "CP_SIZE_READ_10K_100K",  "CP_SIZE_READ_100K_1M",  "CP_SIZE_READ_1M_4M",  "CP_SIZE_READ_4M_10M",  "CP_SIZE_READ_10M_100M",  "CP_SIZE_READ_100M_1G",  "CP_SIZE_READ_1G_PLUS",  "CP_SIZE_READ_AGG_0_100",  "CP_SIZE_READ_AGG_100_1K",  "CP_SIZE_READ_AGG_1K_10K",  "CP_SIZE_READ_AGG_10K_100K",  "CP_SIZE_READ_AGG_100K_1M",  "CP_SIZE_READ_AGG_1M_4M",  "CP_SIZE_READ_AGG_4M_10M",  "CP_SIZE_READ_AGG_10M_100M",  "CP_SIZE_READ_AGG_100M_1G",  "CP_SIZE_READ_AGG_1G_PLUS"]

		#define the size write counters
		dict_counters[module]["size"]["write"] = ["CP_SIZE_WRITE_0_100", "CP_SIZE_WRITE_100_1K", "CP_SIZE_WRITE_1K_10K", "CP_SIZE_WRITE_10K_100K", "CP_SIZE_WRITE_100K_1M", "CP_SIZE_WRITE_1M_4M", "CP_SIZE_WRITE_4M_10M", "CP_SIZE_WRITE_10M_100M", "CP_SIZE_WRITE_100M_1G", "CP_SIZE_WRITE_1G_PLUS", "CP_SIZE_WRITE_AGG_0_100", "CP_SIZE_WRITE_AGG_100_1K", "CP_SIZE_WRITE_AGG_1K_10K", "CP_SIZE_WRITE_AGG_10K_100K", "CP_SIZE_WRITE_AGG_100K_1M", "CP_SIZE_WRITE_AGG_1M_4M", "CP_SIZE_WRITE_AGG_4M_10M", "CP_SIZE_WRITE_AGG_10M_100M", "CP_SIZE_WRITE_AGG_100M_1G", "CP_SIZE_WRITE_AGG_1G_PLUS"]

		#define the access counters
		dict_counters[module]["accesses"] = ["CP_ACCESS1_ACCESS", "CP_ACCESS1_COUNT", "CP_ACCESS2_ACCESS", "CP_ACCESS2_COUNT", "CP_ACCESS3_ACCESS", "CP_ACCESS3_COUNT", "CP_ACCESS4_ACCESS", "CP_ACCESS4_COUNT"]


###########################LOG_VERSION_3_10#######################

	elif log_version == LOG_VERSION_3_10:		
		#in this version, the counter name starts with the module name. Ex: POSIX_F_READ_START_TIMESTAMP, MPIIO_F_WRITE_TIMESTAMP
		#Define the mpi-io dictionary
		module= "mpi-io"
		dict_counters[module]= collections.OrderedDict()

		#define the timestamp read counters
		dict_counters[module]["timestamp"]= collections.OrderedDict()
		dict_counters[module]["timestamp"]["read"]= ["MPIIO_F_READ_START_TIMESTAMP", "MPIIO_F_READ_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["write"]= ["MPIIO_F_WRITE_START_TIMESTAMP", "MPIIO_F_WRITE_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["open"]= ["MPIIO_F_OPEN_TIMESTAMP"]
		#define the timestamp write counters
		dict_counters[module]["timestamp"]["close"]= ["MPIIO_F_CLOSE_TIMESTAMP"]

		#define the time read counters
		dict_counters[module]["time"]= collections.OrderedDict()
		dict_counters[module]["time"]["read"]= ["MPIIO_F_READ_TIME"]

		#define the time write counters
		dict_counters[module]["time"]["write"]= ["MPIIO_F_WRITE_TIME"]

		#define the time meta counters
		dict_counters[module]["time"]["meta"]= ["MPIIO_F_META_TIME"]

		#define the bytes read counters
		dict_counters[module]["bytes"]= collections.OrderedDict()
		dict_counters[module]["bytes"]["read"]= ["MPIIO_BYTES_READ"]

		#define the bytes write counters
		dict_counters[module]["bytes"]["write"]= ["MPIIO_BYTES_WRITTEN"]

		#define the operations counters
		dict_counters[module]["operations"] = ["MPIIO_COLL_OPENS", "MPIIO_COLL_READS", "MPIIO_COLL_WRITES", "MPIIO_INDEP_OPENS", "MPIIO_INDEP_READS", "MPIIO_INDEP_WRITES", "MPIIO_NB_READS", "MPIIO_NB_WRITES", "MPIIO_SPLIT_READS", "MPIIO_SPLIT_WRITES", "MPIIO_SYNCS"]

		#define the performance counters
		dict_counters[module]["performance"] = ["MPIIO_FASTEST_RANK", "MPIIO_FASTEST_RANK_BYTES", "MPIIO_SLOWEST_RANK", "MPIIO_SLOWEST_RANK_BYTES", "MPIIO_F_FASTEST_RANK_TIME", "MPIIO_F_SLOWEST_RANK_TIME"]

		#define the size read counters
		dict_counters[module]["size"]= collections.OrderedDict()
		dict_counters[module]["size"]["read"] = ["MPIIO_SIZE_READ_AGG_0_100", "MPIIO_SIZE_READ_AGG_100_1K", "MPIIO_SIZE_READ_AGG_100K_1M", "MPIIO_SIZE_READ_AGG_100M_1G", "MPIIO_SIZE_READ_AGG_10K_100K", "MPIIO_SIZE_READ_AGG_10M_100M", "MPIIO_SIZE_READ_AGG_1G_PLUS", "MPIIO_SIZE_READ_AGG_1K_10K", "MPIIO_SIZE_READ_AGG_1M_4M", "MPIIO_SIZE_READ_AGG_4M_10M"]

		#define the size write counters
		dict_counters[module]["size"]["write"] = ["MPIIO_SIZE_WRITE_AGG_0_100", "MPIIO_SIZE_WRITE_AGG_100_1K", "MPIIO_SIZE_WRITE_AGG_100K_1M", "MPIIO_SIZE_WRITE_AGG_100M_1G", "MPIIO_SIZE_WRITE_AGG_10K_100K", "MPIIO_SIZE_WRITE_AGG_10M_100M", "MPIIO_SIZE_WRITE_AGG_1G_PLUS", "MPIIO_SIZE_WRITE_AGG_1K_10K", "MPIIO_SIZE_WRITE_AGG_1M_4M", "MPIIO_SIZE_WRITE_AGG_4M_10M"]

		#define the access counters
		dict_counters[module]["accesses"] = ["MPIIO_ACCESS1_ACCESS", "MPIIO_ACCESS1_COUNT", "MPIIO_ACCESS2_ACCESS", "MPIIO_ACCESS2_COUNT", "MPIIO_ACCESS3_ACCESS", "MPIIO_ACCESS3_COUNT", "MPIIO_ACCESS4_ACCESS", "MPIIO_ACCESS4_COUNT"]

		########################################################################

		#Define the posix dictionary
		module= "posix"
		dict_counters[module]= collections.OrderedDict()

		#define the timestamp read counters
		dict_counters[module]["timestamp"]= collections.OrderedDict()
		dict_counters[module]["timestamp"]["read"]= ["POSIX_F_READ_START_TIMESTAMP", "POSIX_F_READ_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["write"]= ["POSIX_F_WRITE_START_TIMESTAMP", "POSIX_F_WRITE_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["open"]= ["POSIX_F_OPEN_START_TIMESTAMP", "POSIX_F_OPEN_END_TIMESTAMP"]
		#define the timestamp write counters
		dict_counters[module]["timestamp"]["close"]= ["POSIX_F_CLOSE_START_TIMESTAMP", "POSIX_F_CLOSE_END_TIMESTAMP"]

		#define the time read counters
		dict_counters[module]["time"]= collections.OrderedDict()
		dict_counters[module]["time"]["read"]= ["POSIX_F_READ_TIME"]

		#define the time write counters
		dict_counters[module]["time"]["write"]= ["POSIX_F_WRITE_TIME"]

		#define the time meta counters
		dict_counters[module]["time"]["meta"]= ["POSIX_F_META_TIME"]

		#define the bytes read counters
		dict_counters[module]["bytes"]= collections.OrderedDict()
		dict_counters[module]["bytes"]["read"]= ["POSIX_BYTES_READ"]

		#define the bytes write counters
		dict_counters[module]["bytes"]["write"]= ["POSIX_BYTES_WRITTEN"]

		#define the operations counters
		dict_counters[module]["operations"] = ["POSIX_OPENS", "POSIX_READS", "POSIX_WRITES", "POSIX_SEEKS", "POSIX_CONSEC_READS", "POSIX_CONSEC_WRITES", "POSIX_SEQ_READS", "POSIX_SEQ_WRITES"]

		#define the performance counters
		dict_counters[module]["performance"] = ["POSIX_FASTEST_RANK", "POSIX_FASTEST_RANK_BYTES", "POSIX_SLOWEST_RANK", "POSIX_SLOWEST_RANK_BYTES", "POSIX_F_FASTEST_RANK_TIME", "POSIX_F_SLOWEST_RANK_TIME"]

		#define the size read counters
		dict_counters[module]["size"]= collections.OrderedDict()
		dict_counters[module]["size"]["read"] = ["POSIX_SIZE_READ_0_100",  "POSIX_SIZE_READ_100_1K",  "POSIX_SIZE_READ_100K_1M",  "POSIX_SIZE_READ_100M_1G",  "POSIX_SIZE_READ_10K_100K",  "POSIX_SIZE_READ_10M_100M",  "POSIX_SIZE_READ_1G_PLUS",  "POSIX_SIZE_READ_1K_10K",  "POSIX_SIZE_READ_1M_4M",  "POSIX_SIZE_READ_4M_10M"]

		#define the size write counters
		dict_counters[module]["size"]["write"] = ["POSIX_SIZE_WRITE_0_100", "POSIX_SIZE_WRITE_100_1K", "POSIX_SIZE_WRITE_100K_1M", "POSIX_SIZE_WRITE_100M_1G", "POSIX_SIZE_WRITE_10K_100K", "POSIX_SIZE_WRITE_10M_100M", "POSIX_SIZE_WRITE_1G_PLUS", "POSIX_SIZE_WRITE_1K_10K", "POSIX_SIZE_WRITE_1M_4M", "POSIX_SIZE_WRITE_4M_10M"]

		#define the access counters
		dict_counters[module]["accesses"] = ["POSIX_ACCESS1_ACCESS", "POSIX_ACCESS1_COUNT", "POSIX_ACCESS2_ACCESS", "POSIX_ACCESS2_COUNT", "POSIX_ACCESS3_ACCESS", "POSIX_ACCESS3_COUNT", "POSIX_ACCESS4_ACCESS", "POSIX_ACCESS4_COUNT"]

		########################################################################

		#Define the stdio dictionary
		module= "stdio"
		dict_counters[module]= collections.OrderedDict()

		#define the timestamp read counters
		dict_counters[module]["timestamp"]= collections.OrderedDict()
		dict_counters[module]["timestamp"]["read"]= ["STDIO_F_READ_START_TIMESTAMP", "STDIO_F_READ_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["write"]= ["STDIO_F_WRITE_START_TIMESTAMP", "STDIO_F_WRITE_END_TIMESTAMP"]

		#define the timestamp write counters
		dict_counters[module]["timestamp"]["open"]= ["STDIO_F_OPEN_START_TIMESTAMP", "STDIO_F_OPEN_END_TIMESTAMP"]
		#define the timestamp write counters
		dict_counters[module]["timestamp"]["close"]= ["STDIO_F_CLOSE_START_TIMESTAMP", "STDIO_F_CLOSE_END_TIMESTAMP"]


		#define the time read counters
		dict_counters[module]["time"]= collections.OrderedDict()
		dict_counters[module]["time"]["read"]= ["STDIO_F_READ_TIME"]

		#define the time write counters
		dict_counters[module]["time"]["write"]= ["STDIO_F_WRITE_TIME"]

		#define the time meta counters
		dict_counters[module]["time"]["meta"]= ["STDIO_F_META_TIME"]

		#define the bytes read counters
		dict_counters[module]["bytes"]= collections.OrderedDict()
		dict_counters[module]["bytes"]["read"]= ["STDIO_BYTES_READ"]

		#define the bytes write counters
		dict_counters[module]["bytes"]["write"]= ["STDIO_BYTES_WRITTEN"]

		#define the operations counters
		dict_counters[module]["operations"] = ["STDIO_OPENS", "STDIO_READS", "STDIO_WRITES", "STDIO_SEEKS"]

		#define the performance counters
		dict_counters[module]["performance"] = ["STDIO_FASTEST_RANK", "STDIO_FASTEST_RANK_BYTES", "STDIO_SLOWEST_RANK", "STDIO_SLOWEST_RANK_BYTES", "STDIO_F_FASTEST_RANK_TIME", "STDIO_F_SLOWEST_RANK_TIME"]

		#define the size counters
		dict_counters[module]["size"]= collections.OrderedDict()
		dict_counters[module]["size"]["read"] = []
		dict_counters[module]["size"]["write"] = []

		#define the access counters
		dict_counters[module]["accesses"] = []

		########################################################################

		#Define the lustre module dictionary
		module= "lustre"
		dict_counters[module]= collections.OrderedDict()

		#define the info counters
		dict_counters[module]["operations"] = ["LUSTRE_OSTS", "LUSTRE_MDTS", "LUSTRE_STRIPE_OFFSET", "LUSTRE_STRIPE_SIZE", "LUSTRE_STRIPE_WIDTH"]

		#define the timestamp counters
		dict_counters[module]["timestamp"]= collections.OrderedDict()
		dict_counters[module]["timestamp"]["read"]= []
		dict_counters[module]["timestamp"]["write"]= []
		dict_counters[module]["timestamp"]["open"]= []
		dict_counters[module]["timestamp"]["close"]= []

		#define the time counters
		dict_counters[module]["time"]= collections.OrderedDict()
		dict_counters[module]["time"]["read"]= []
		dict_counters[module]["time"]["write"]= []
		dict_counters[module]["time"]["meta"]= []

		#define the timestamp counters
		dict_counters[module]["bytes"]= collections.OrderedDict()
		dict_counters[module]["bytes"]["read"]= []
		dict_counters[module]["bytes"]["write"]= []

		#define the size counters
		dict_counters[module]["size"]= collections.OrderedDict()
		dict_counters[module]["size"]["read"] = []
		dict_counters[module]["size"]["write"] = []

		#define the performance counters
		dict_counters[module]["performance"] = []

		#define the access counters
		dict_counters[module]["accesses"] = []

	return dict_counters

# Function that writes the compress json output file
# in: darshan log file name
# Modification date: 2018-05-22
# Mod Author: Pablo José Pavan
# Output filename = filename.json

def save_json(filename,dict_header,dict_contents):
	dict_json = collections.OrderedDict()
	dict_json.update(dict_header)
	dict_json.update(dict_contents)
	file_json = json.dumps(dict_json)
	compress_json = zlib.compress(json.dumps(file_json))
	f = open(filename+".json", "w")
	f.write(compress_json)
	f.close()


def main():

	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--file', "-f",required=True,help= "Input darshan parsed file")

	args = parser.parse_args()

	dict_contents = collections.OrderedDict()
	dict_header = collections.OrderedDict()
	dict_counters = collections.OrderedDict()
	
	filename = args.file

	dict_header = read_darshan_header(filename)
	dict_counters = create_counter_structure(dict_header["header"]["darshan"])

	if dict_header["header"]["darshan"] >= LOG_VERSION_3_10:		
		dict_contents = read_darshan_file_new(filename, dict_counters)
	else:
		dict_contents = read_darshan_file_old(filename, dict_counters["all"])
	save_json(filename,dict_header,dict_contents)
	return 0

if __name__ == "__main__":
	main()
